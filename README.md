# OML4

* cours G.Soranzo : https://github.com/gsoranzo/gsoranzo.github.io   S29.pdf
* Contenus privés: https://gitlab.com/hazaa/oml4_private


## install

* uninstall `pip uninstall notebook`
*  create a mamba environment then  `mamba create -n nameof env <list of packages> `, i.e. `mamba create -n jb`  [mamba](https://mamba.readthedocs.io/en/latest/user_guide/mamba.html#quickstart)
* activate `mamba activate jb`
* install `mamba install -c conda-forge jupyter-book notebook sphinx-proof numpy matplotlib scipy`
* install `pip install sphinx-proof`
* config proof extension [jb](https://jupyterbook.org/en/stable/content/proof.html#adding-extension-through-config-yml)

## TODO

* TODO: Appliquer les matrices à la géométrie/rotation
