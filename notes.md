* dec 2024
  * nouveau repertoire oml4/latex: pour héberger le chapitre DFT, 
    (auparavant traité en OML3)
  *  TODO:
      * paser en latex; passer les TD en mode 2 colonnes
	ajouter 4eme TD
      * motivating exemple: analyse de spectre
      * ajouter calcul de la valeur efficace (avant TDH)
      * ajouter reconsruction exacte du signal
      * ajouter cas avec bruit

* sept 2024:
  * nouveau repo: oml3_latex
  * manuels: larnal (maths dunod), chateignier (génie elec, dunod)
  * enlever/modifier: SFD, filtres
  * manuel :
  * ajouter: polynôme matriciel; méth cramer ; (rang matrice)
    (algebre: applic lin; noyau, image, ...) 
  * ajouter DSF. 
    nouveautés par rapport à OML2: 
      période quelconque (pas pi)
      signaux triangle, dent de scie...
      parseval,TDH
    compétences de base: exploiter la symétrie par rapport à y=0 (e.g. intégrale nulle), exploiter symétrie x=0 (2*int)
    prolongmeent: écriture complexe $c_n$ (exo: chateignier p.100)
    nouveaux exemples: 
      puissance dissipée (chateignier p.101) 
      transmission puissance (en fréquentiel)	

* ajouter rotations+complexes+fresnel:
   https://fr.wikipedia.org/wiki/Rotation_vectorielle
   http://electropratic.free.fr/puissances.html
   http://www.iutenligne.net/resources.html?text=matrice&diplome=23682&domaines=26620&ct=1&ca=1&cc=1&ci=1&ck=1&title=matrice&resume=matrice&keywords=matrice&code=matrice&aname=matrice&asname=matrice&lrt=All&level=All
   https://www.youtube.com/watch?v=vZFM1KyokO8

* matrices: produit scalaire entre 2 vecteurs: ```np.sum(a*b) ```
  exemple: DFT, oml3 TP2
  a voir avec gabriel
            
* matrices: multiplication: base de fourier. DFT. cf Unpingco
  cf tp carrano [github](https://github.com/dominiccarrano/ee-120-labs)
  [matlab](https://www.princeton.edu/~cuff/ele301/labs.html)

  unpingco 2.2 reconstructing. équivaut à DFT.

* matrice: résolution de système: 
  ex: loi de Kirchhoff du courant, sous forme nodal. cf Mohan p.80
  I = YU

  POnt de wheatstone unisciel warrin

* matrices: opale: https://www.dropbox.com/home/Opale/Sauvegardes/MATHGEII20170629/Contenus/Grains/Matrices
https://www.dropbox.com/home/Opale/MATHGEII/Contenus/Grains/Matrices

* transformée en Z ?? group delay ?
  * parallèle avec domaine continu,
  * résolution d'une eq aux différences: d'abord tf, puis DES puis tf inverse
  * table des tf
  * réponse en freq (amplitude et phase)
  * refs:
  cormier: https://www8.umoncton.ca/umcm-cormier_gabriel/Signaux/GELE2511_Chapitre8.pdf
  garnier: http://w3.cran.univ-lorraine.fr/perso/hugues.garnier/Enseignement/Auto_num/B-Transformee_Z.pdf
  https://www.bibmath.net/dico/index.php?action=affiche&quoi=./t/transformeez.html
