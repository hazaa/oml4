# TD1 Matrices


1 Lecture des éléments d’une matrice
-----

Soit la matrice:

$$
\mathbf{A} = \left(\begin{array}{ccc} 0 & 1 & 2   \\ -1 & 2 & 5 \\ -2 & 3 & 0\end{array} \right)
$$

Déterminez les valeurs de $a_{11}, ~a_{21}, ~a_{23}, ~a_{32}$.


2 Dimension des matrices
------

Déterminer la dimension $(n,p)$ des matrices suivantes :

$$
\mathbf{A} = \left(\begin{array}{ccc} 0 & 1 & 2 \end{array} \right)
~\mathbf{B} = \left(\begin{array}{cc} 0 & 1    \\ 1 & 1 \\ 2 & 1\end{array} \right)
~\mathbf{C} = \left(\begin{array}{ccc} 0 & 1 & 2   \\ -1 & 2 & 5 \\ -2 & 3 & 0\end{array} \right)
$$



3 Matrices particulières
------

Comment se nomment les matrices particulières suivantes ?

$$
\mathbf{A} = \left(\begin{array}{ccc} 0 & 1 & 2 \end{array} \right)
~\mathbf{B} = \left(\begin{array}{c} 2  \\ 1 \\ 0 \end{array} \right)
~\mathbf{C} = \left(\begin{array}{ccc} 0 & 1 & 2\\0 & 1 & 2\\0 & 1 & 2\end{array} \right)
~\mathbf{D} =  \left(\begin{array}{ccc} 1 & 0 & 0   \\0 & 1 & 0  \\ 0 & 0 & 1  \end{array} \right)
~\mathbf{E} = \left(\begin{array}{ccc} 3 & 1 & 2\\0 & 1 & 2\\0 & 0 & 2\end{array} \right)
$$


4 Addition, soustraction, multiplication par un scalaire, transposition
-----

Soient les matrices $A$, et $B$ ci-dessous et le scalaire $k = 3$.

$$
\mathbf{A} = \left(\begin{array}{cc} 0 & 1    \\ -1 & 2 \\ -2 & 3\end{array} \right)
~\mathbf{B} = \left(\begin{array}{cc} 3 & -2    \\ 0 & 1 \\ 1 & 2\end{array} \right)
$$

Calculer:

1. $A+B, ~A-B, ~kA$
1. Calculer $^tA, ~^t(k ^tA)$
1. Calculer $A +^tB$


5 Multiplication des matrices
-----

Soient les matrices $A, ~B, ~C$ ci-dessous:

$$
\mathbf{A} = \left(\begin{array}{cc} -5 & 5\end{array} \right)
~\mathbf{B} = \left(\begin{array}{cccc} 1 & 5 &7 & -3  \\ -7  & 8 & -9 & 0\end{array} \right)
~\mathbf{C} = \left(\begin{array}{cc} -3 & -1    \\ 0 & 1\end{array} \right)
~\mathbf{I}_2 = \left(\begin{array}{cc} 1 & 0    \\ 0 & 1\end{array} \right)
$$

1. Quels sont le produits réalisables?
1. Calculer $AB$, $AC$, $CB$.
1. Puissance de matrice : calculer $C^3$.
1. Calculer $^tCC$ et $C ^tC$. La multiplication est-elle commutative dans ce cas?

6 Produits scalaires
-----

Donner la dimension de l'espace, calculer le produit scalaire des vecteurs, et dire si les vecteurs sont orthogonaux :

1. $\vec{x}=[1,0]^T$ et $\vec{y}=[0,1]^T$
1. $\vec{x}=[0,0,1]^T$ et $\vec{y}=[0,1,0]^T$
1. $\vec{x}=[0,1,2]^T$ et $\vec{y}=[4,5,6]^T$

7 Calcul de l’inverse par l’utilisation d’un polynôme annulateur
-----

Soit une matrice $A$ de $M^n(\mathbb{R})$, et $I_n$ la matrice identité.
On suppose que $A$ vérifie la relation $A^2 +3A−I_n =0$


1.  Montrer que $A$ est inversible.
1.  Déterminer $A^{−1}$.
1.  Vérifier que l’on a également $A^{−1}A=I_n$


8 Calcul de l’inverse par l’utilisation d’un polynôme annulateur
-----

Soit $A$ la matrice carrée (3,3) suivante:

$$
~\mathbf{A} = \left(\begin{array}{ccc} 1 & 1 & 0\\0 & 1 & 1\\0 & 0 & 1\end{array} \right)
$$

1.  Calculer $(A-I)^3$ sous forme matricielle. 
1.  En déduire une relation entre $A^3, A^2, A$ et $I$ en développant l'expression ci-dessus. On pourra utiliser les coefficients du triangle de Pascal, rappelés ci-dessous.
1.  Montrer que $A$ est inversible et déterminer $A^{-1}$ sous forme d’un polynôme en $A$ puis sous forme matricielle.
1.  Vérifier que $AA^{-1} = I$ et $A^{-1}A=I$ sous forme matricielle.

$$
\begin{array}{cccc} 1 &  && \\1 & 1 && \\1 & 2 & 1& \\1 & 3 &3 & 1 \end{array}
$$

9 Calcul de l’inverse par la comatrice
-----

Soit $A$ la matrice carrée (2,2) suivante :

$$
\mathbf{A} = \left(\begin{array}{cc} 1 & 1    \\ 2 & -1\end{array} \right)
$$

1.   Calculer $det(A)$. $A$ est-elle inversible ? 
1.   Calculer $A^{-1}$ par la méthode de la comatrice.
1.   Vérifier que $AA^{-1} = I$ et $A^{-1}A=I$.
