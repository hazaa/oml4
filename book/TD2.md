# TD2 Matrices


1 Résolution d’un système linéaire sous forme matricielle
-----

Soit le système:

$$
(S) :\left\{
    \begin{array}{cccccccc} 
    2x & + & 2y & + & 3z &=& a  &L1  \\ 
    x & + & -y &  &  &=& b  &L2\\
    -x & + & 2y & + & z &=& c  &L3\\
    \end{array} \right. 
$$

1.  Ecrire $(S)$ à l’aide d’un produit de matrices $AX = B$, où $X$ et $B$ sont des matrices colonnes.
1.  Résoudre $(S)$ par la méthode du pivot de Gauss.
1.  Calculer le déterminant de $A$. $A$ est-elle inversible?
1.  Calculer $A^{-1}$ par la formule de la comatrice.
1.  Vérifier que $A A^{−1} = A^{−1}A=I_n$
1.  Résoudre le système grâce à $A^{-1}$. Comparer votre solution à celle trouvée plus haut.
1.  BONUS: calculer $A^{-1}$ par la méthode de Gauss.


2 Résolution d’un système linéaire sous forme matricielle
-----

Reproduire le schéma complet de l’exercice 1 pour le système suivant. 

$$
\mathbf{A} = \left(\begin{array}{ccc} 1& 2 & -3 \\ 2 & -2 & -1 \\ 3 & 2 & 1 \\\end{array} \right)
~\mathbf{B} =\left(\begin{array}{c} 16 \\ 6 \\ 4 \\\end{array} \right)
$$


3 Application GEII
-----

Soient deux quadripôles linéaires suivants, dont on donne le schéma Fig.{ref}`circuit-fig` et les représentations matricielles ci-dessous.

$
 \begin{equation*}
   \left(
   \begin{array}{c}
        i_2  \\
        e_2 \\
    \end{array} \right)=
   \left(
   \begin{array}{cc}
     1 & 0\\
     -Z_1 & 1  \\
    \end{array} \right)
   \left(
   \begin{array}{c}
        i_1  \\
        e_1 \\
    \end{array} \right),  
   \left(
   \begin{array}{c}
        i_2  \\
        e_2 \\
    \end{array} \right)=
   \left(
   \begin{array}{cc}
     1 & -1/Z_2\\
     0 & 1  \\
    \end{array} \right)
   \left(
   \begin{array}{c}
        i_1  \\
        e_1 \\
    \end{array} \right),   
  \end{equation*}
$  


1. Pour le circuit de gauche de la Fig. {ref}`circuit-fig`, écrire la loi de mailles et la loi des noeuds, puis vérifier que le résultat correspond à celui de la représentation matricielle.
1. idem pour le circuit de droite de la Fig.{ref}`circuit-fig`.
1. Calculer les représentations matricielles des deux circuits (en T et en $\Pi$) de la Fig.{ref}`circuit_T_pi-fig`.
1. Pour le circuit en T, faire tendre $R_2$ vers $+\infty$. La représentation matricielle est-elle cohérente ?
1. Pour le circuit en $\Pi$, faire tendre $R_1,~R_3$ vers $+\infty$. La représentation matricielle est-elle cohérente ?




```{figure} fig/circuit.svg
---
height: 150px
name: circuit-fig
---
(a)
```

```{figure} fig/circuits_T_pi.svg
---
height: 150px
name: circuit_T_pi-fig
---
(b)
```


Source [wikipedia](https://fr.wikipedia.org/wiki/Quadrip%C3%B4le)
