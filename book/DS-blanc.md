# DS blanc 2

Attention:

* Les documents et téléphones sont interdits.
* L'usage des calculatrices collège est autorisé.
* Durée: 1h30


Exercice 1 Matrice $2\times 2$ (2 points)
----

Soit la matrice:

$$
\mathbf{A} = \left(
\begin{array}{cc}
1 & 0  \\
2 & 1 \\
\end{array} \right)
$$

1.  Calculer $\det(A)$. $A$ est-elle inversible ?
1.  Calculer $A^{-1}$. 
1.  Calculer $A^{-1}A$. 

Exercice 2 Produit scalaire (2 points)
----

Soient $\vec{a}$ et $\vec{b}$ des vecteurs en dimension 2.
$~\vec{b}$ est la rotation du vecteur $\vec{a}$, d'un angle $\theta$ dans le sens trigonométrique.

1. Rappeler la formule du produit scalaire de deux vecteurs en dimension $n$, puis en dimension 2.
1. Rappeler la formule de la rotation sous forme matricielle en dimension 2
1. Pour $\theta$ quelconque, calculer $\vec{a}.\vec{b}$.
1. Application numérique: $\vec{a}=[1,0]^T, ~\theta \in [0, \frac{\pi}{4},\frac{\pi}{2} ]$

Exercice 3 Produit scalaire et SFD (3 points)
----

On veut savoir si un vecteur $\vec{x}$ est proche d'un vecteur $\vec{v}_k$, ou
si au contraire ils sont orthogonaux. On va calculer leur produit scalaire.

$N=8, ~\vec{v}_k=[\sin(2 \pi \frac{k}{N} 0), \sin(2 \pi \frac{k}{N} 1), \ldots, \sin(2 \pi \frac{k}{N} n), \ldots, \sin(2 \pi \frac{k}{N} (N-1))  ]^T$


1. Calculer $\vec{v}_0, ~\vec{v}_1, ~\vec{v}_2$
1. Calculer $\vec{x}.\vec{v}_k$ pour $k \in [0,1,2]$
1. De quel $\vec{v}_k$ le vecteur $\vec{x}$ est-il le plus proche ? ($\vec{x}$ est donné ci-dessous). 

$x=[-0.0, 0.72, 0.99, 0.71, 0.01, -0.7, -0.98, -0.71]$


Exercice 4 Formule de la comatrice (3 points)
----

1. Rappeler la formule de l'inverse par la méthode de la comatrice
1. Calculer l'inverse de $A$ par la méthode de la comatrice.

$$
\mathbf{A} = \left(
\begin{array}{ccc}
1 & 1 & 1 \\
2 & -2 & -1 \\
3 & 2 & 1 
\end{array} \right)
$$


Exercice 5 Polynôme matriciel et inversion (5 points)
----

$$
\mathbf{A} = \left(
\begin{array}{ccc}
-1 & 3 & -3 \\
0 & -2 & 1 \\
0 & 0 & 1 
\end{array} \right)
$$

1.  Calculer $\det(A)$. $A$ est-elle inversible ?
1.  Calculer $A^3+2A^2-A$ et montrer que le résultat est un multiple de la matrice identité.
1.  En déduire $A^{-1}$ en fonction de $A^2, ~A, ~I$.
1.  En déduire l'expression matricielle de $A^{-1}$
1.  Calculer $A \cdot A^{-1}$
1.  Calculer $A^{-1}$ par la formule de la comatrice.



Exercice 6 Résolution de système linéaire (4 points)
----

Soit le système linéaire $(S):\mathbf{A} \mathbf{X}=\mathbf{B} $ où $\mathbf{A}$ est donnée ci-dessous et où:

$$
\mathbf{X} =\left(\begin{array}{c} x \\ y \\ z \\\end{array} \right)
~\mathbf{B} =\left(\begin{array}{c} a \\ b \\ c \\\end{array} \right)
$$

1. Résoudre $(S)$ à la main en partant de la dernière ligne et en remontant.

$$
\mathbf{A} = \left(
\begin{array}{ccc}
-1 & 3 & -3 \\
0 & -2 & 1 \\
0 & 0 & 1 
\end{array} \right)
$$

Exercice 7 Résolution de système linéaire par pivot de Gauss (BONUS)
----

Résoudre $(S):\mathbf{A} \mathbf{X}=\mathbf{B} $ par la méthode du pivot de Gauss.

$$
\mathbf{A} = \left(\begin{array}{ccc} 1& 1 & 1 \\ 2 & -2 & -1 \\ 3 & 2 & 1 \\\end{array} \right)
~\mathbf{B} =\left(\begin{array}{c} 1 \\ 0 \\ 1 \\\end{array} \right)
$$





