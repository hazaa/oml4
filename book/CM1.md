#  CM Transformée en Z

`````{admonition} Prérequis

* OML3, chapitre 1 (Laplace).
* OML3, chapitre 1 (Série entière: $\sum x^k=1/(1-x)$).
*  OML3, chapitre 3 (TZ).

`````

```{danger}
* Dans ce chapitre le temps n'est plus **continu** mais **discret**: on le notera $k \in \mathbb{N}$, ou $n \in \mathbb{N}$. 
* On ne calcule plus des **intégrales** $\int x(t)dt$ mais des **sommes** $\sum_k x_k$.
```

```{note}
Dans cette partie du cours nous nous appuyons sur le livre de F.Cottet {cite}`cottet_2020`, disponible en format électronique à la bibliothèque universitaire de l'UPEC.
```



Transformée en Z inverse
-----

bla

Table

Comparaison avec la Transformée de Laplace (temps continu)

Résolution d'une équation aux différences
-----

Comparaison avec la Transformée de Laplace (temps continu)
