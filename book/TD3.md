# TD3 DS blanc

Attention:

* Les documents et téléphones sont interdits.
* L'usage des calculatrices collège est autorisé.
* Durée: 1h30


Exercice 1 Matrice $2\times 2$ (2 points)
----

Soit la matrice:

$$
\mathbf{A} = \left(
\begin{array}{cc}
1 & 0  \\
2 & 1 \\
\end{array} \right)
$$

1.  Calculer $\det(A)$. $A$ est-elle inversible ?
1.  Calculer $A^{-1}$. 
1.  Calculer $A^{-1}A$. 

Exercice 2 Polynôme matriciel et inversion (5 points)
----

$$
\mathbf{A} = \left(
\begin{array}{ccc}
-1 & 3 & -3 \\
0 & -2 & 1 \\
0 & 0 & 1 
\end{array} \right)
$$

1.  Calculer $\det(A)$. $A$ est-elle inversible ?
1.  Calculer $A^3+2A^2-A$ et montrer que le résultat est un multiple de la matrice identité.
1.  En déduire $A^{-1}$ en fonction de $A^2, ~A, ~I$.
1.  En déduire l'expression matricielle de $A^{-1}$
1.  Calculer $A \cdot A^{-1}$


Exercice 3 Résolution de système linéaire (4 points)
----

Soit le système linéaire $(S):\mathbf{A} \mathbf{X}=\mathbf{B} $ où $\mathbf{A}$ est donné par l'exercice précédent et où:

$$
\mathbf{X} =\left(\begin{array}{c} x \\ y \\ z \\\end{array} \right)
~\mathbf{B} =\left(\begin{array}{c} a \\ b \\ c \\\end{array} \right)
$$

1. Résoudre $(S)$ à la main.
1. Résoudre $(S)$ à l'aide de $\mathbf{A}^{-1}$ déjà calculée dans l'exercice précédent .


Exercice 4 Résolution de système linéaire par pivot de Gauss (6 points)
----

Résoudre $(S):\mathbf{A} \mathbf{X}=\mathbf{B} $ par la méthode du pivot de Gauss.

$$
\mathbf{A} = \left(\begin{array}{ccc} 1& 1 & 1 \\ 2 & -2 & -1 \\ 3 & 2 & 1 \\\end{array} \right)
~\mathbf{B} =\left(\begin{array}{c} 1 \\ 0 \\ 1 \\\end{array} \right)
$$

Exercice 5 Applications GEII: circuit électrique (3 points)
----

Soit un circuit électrique dans lequel on a écrit la loi des noeuds:

* noeud A: $I=I_1+I_2$
* noeud B: $I_1=I_3+I_4$ 
* noeud C: $I_5=I_2+I_3$
* noeud D: $I=I_4+I_5$

et la loi des mailles:

* maille ABCA: $R_1 I_1 + R_3 I_3-R_2 I_2=0$
* maille BDCB: $R_4 I_4 -R_5 I_5 -R_3 I_3=0$
* maille ACDA: $R_2 I_2+R_5 I_5=E$ 

En substituant $I_1$ et $I_5$ dans les équations de maille, écrire les équations sous forme d'un système linéaire matriciel $\mathbf{Ax}= \mathbf{b}$, avec $\mathbf{x}=[I_2,I_3,I_4]^T$.



