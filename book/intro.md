# Outils Mathématiques et Logiciels (OML4)

**Programme national (PN)**
%-----

Extrait du programme national (PN) LP-BUT GEII 2022, R4.04:

Les thèmes recommandés à développer pour atteindre les acquis d’apprentissage visés sont :


* Matrices : Définition, propriétés, formules usuelles :
  * Effectuer les calculs matriciel de base : opérations de base, inversion, calcul du déterminant ;
  * Utiliser les matrices pour résoudre un système d’équation ;
  * Appliquer les matrices à la géométrie/rotation.
* (Transformation en Z : Suites géométriques et arithmétiques (généralités), séries géométriques et exponentielles (généralités) )
  * (Résoudre une équation aux différences );
  * (Déterminer la fonction de transfert d’un système (filtre) et sa réponse.)

%**Plan du cours**
%-----

%```{tableofcontents}
%```

**Planning 2023-2024**
%-----

| Nature| Thème  | Evaluation | A | B | FA2 A1 |
| ---      | ---      | ---      | ---      | ---      |---      |
| CM1   | Matrices 1 |  | 25/01 |  |  |
| CM2   | Matrices 2|  | 25/01 |  |  |
| TD1   | Matrices 1| |26/01 |  |  |  
| TD2   |  Matrices 2| | 26/01 | |  |
| TD3   | DS blanc |  |  05/03|  |  |
| **DS**   |  |  | 26/03 |  |  |

| Nature| Thème  | Evaluation | A1 | A2 | B1 | FA2 A1 |
| ---      | ---      | ---      | ---      | ---      |---      |---|
| TP1   | Matrices 1| quiz |  |   |  | |
| TP2   | Matrices 2| CTP 2h | |  |  | |


