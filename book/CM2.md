#  CM Matrices


```{note}
L'auteur de l'essentiel de ce chapitre est Guillaume Bourlet. 

Les corrections, l'adaptation depuis Opale sous Jupyter-book, et les exemples GEII à la fin sont de A.Hazan.

La méthode du pivot de Gauss est tirée d'une autre source mentionnée ci-dessous.

Pour aller plus loin, vous pouvez consulter {cite}`warin_1999`.
```

`````{admonition} Prérequis
* cours OML2, matrices: chapitre 9, `S29.pdf`
`````

Introduction
--------------

Les matrices sont des tableaux bidimensionnels de nombre réels.

````{prf:example}
**Une matrice (4,2) : 4 lignes, 2 colonnes**

$$
\begin{equation*}\mathbf{M} = \left(\begin{array}{cc} 0 & 1  \\ \colorbox{yellow} {-6} & 2  \\ 1 & \colorbox{green} 8  \\-5 & 7 \end{array} \right)\end{equation*}
$$

On note:

* $m_{i,j}$ le terme générique 
* $i$ est l'indice de ligne ;
* $j$ l'indice de colonne.

$m_{2,1} = \colorbox{yellow} {-6}$  est à l'intersection de la 2ème ligne et la 1ère colonne.

$m_{3,2} = \colorbox{green}{8}$ est à l'intersection de la 3ème ligne et la 2ème colonne.
````


````{prf:example}
**Une matrice (3,3) : 3 lignes, 3 colonnes**

$$
\begin{equation*}\mathbf{A} = \left(\begin{array}{ccc}0 & 1 & 2 \\-1 & 2 & 5 \\-2 & 3 & 0 \\\end{array} \right)\end{equation*}
$$


$a_{3,2} = 3$

Les matrices ayant le même nombre de lignes et de colonnes sont appelées **matrices carrées**.

````

Le vocabulaire des matrices
-------

````{prf:definition}
:label: matrice
 
**Matrice**:

Une matrice $\color{blue} M(n,p)$ est un tableau bidimensionnel de nombres réels.

Ce tableau compte $n$ lignes et $p$ colonnes.

On note:

* $\color{blue} m_{i,j}$  le terme générique de la matrice $M$.
* $i$ l'indice de ligne.
* $j$ l'indice de colonne.


````


````{prf:definition}
:label: matrice-transposée
 
**Matrice transposée**:

La transposée d'une matrice  $M(n,p)$  est la matrice notée $^tM(p,n)$  dans laquelle les lignes et les colonnes de M ont été échangées.

**Attention**: on trouve aussi la notation $M^T$
````

````{prf:example}
**Transposée**

Soit :

$$
\begin{equation*}\mathbf{A} = \left(\begin{array}{ccc} 0 & -3 & 6  \\-1 & 2 &  4 \\-2 & 3 & 8 \\  {- 5} & 7 & 9\end{array} \right)\end{equation*}
$$

Alors:

$$
\begin{equation*}^t\mathbf{A} = \left(\begin{array}{cccc} 0 & -1 & -2 & -5  \\-3 & 2 &  3 & 7\\6 & 4 & 8 & 9\end{array} \right)\end{equation*}
$$

````


````{prf:definition}
:label: matrice-carree
 
**Matrice carrée**:

Une matrice est carrée si elle a même nombre de lignes que de colonnes.

````

````{prf:example}

La matrice $A(4,3)$ n'est pas carrée : 

$$
\begin{equation*}\mathbf{A} = \left(\begin{array}{ccc} 0 & -3 & 6  \\-1 & 2 &  4 \\-2 & 3 & 8 \\  {- 5} & 7 & 9\end{array} \right)\end{equation*}
$$

La matrice $B(4,4)$ est carrée:

$$
\begin{equation*}\mathbf{B} = \left(\begin{array}{cccc} 0 & -1 & -2 & -5  \\-3 & 2 &  3 & 7 \\ 6 & 4 & 8 & 9 \\ -6 & 2 & 3 & 11 \end{array} \right)\end{equation*}
$$

````


````{prf:definition}
:label: matrice-sa-diagonale
 
**Diagonale d'une matrice**:

La diagonale d'une matrice $A(n,p)$ est l'ensemble des coefficients de la forme $a_{i,i}$, c'est à dire ceux dont l'indice de ligne est égal à l'indice de colonne.

````

````{prf:example}

Deux matrices et leur diagonale surlignée.

$$
\mathbf{A} = \left(\begin{array}{cccc} \colorbox{yellow} 0 & -1 & -2 \\-1 & \colorbox{yellow} 4 & 5 \\ -2 & 5 & \colorbox{yellow}8 \\ -6 & 2 & 9 \end{array} \right)
$$

$$
\begin{equation*}\mathbf{B} = \left(\begin{array}{cccc} \colorbox{yellow} 0 & -1 & -2 & -6  \\-1 & \colorbox{yellow} 4 & 5 & 2 \\ -2 & 5 & \colorbox{yellow}8 & 9 \\ -6 & 2 & 9 & \colorbox{yellow} {11} \end{array} \right)\end{equation*}
$$
````


````{prf:definition}
:label: matrice-symetrique
 
**Matrice symétrique**:

Une matrice est symétrique si elle est égale à sa transposée, ce qui signifie que les termes symétriques par rapport à la diagonale sont égaux.

Une matrice symétrique est donc nécessairement carrée.
````

````{prf:example}
La matrice $B(4,4)$ est symétrique :

$$
\begin{equation*}\mathbf{B} = \left(\begin{array}{cccc} 0 & \colorbox{yellow} {-1} & \colorbox{green} {-2} & \colorbox{cyan} {-6}  \\\colorbox{yellow} {-1} & 4 & \colorbox{yellow} 5 & \colorbox{green}2 \\ \colorbox{green} {-2} & \colorbox{yellow} 5 & 8 & \colorbox{yellow} 9 \\ \colorbox{cyan} {-6} & \colorbox{green}2 & \colorbox{yellow} 9 & 11 \end{array} \right)\end{equation*}
$$
````

````{prf:definition}
:label: matrice-triangulaire
 
**Matrice triangulaire**:

Une matrice est triangulaire si les nombres en-dessous ou au-dessus de la diagonale sont nuls.
````

````{prf:example}

La matrice $A(4,4)$ est triangulaire supérieure car les coefficients sous la diagonale sont nuls :

$$
\begin{equation*}\mathbf{A} = \left(\begin{array}{cccc} 3 & -1 & -2 & -6  \\\colorbox{yellow} 0 & 4 & 5 & 0 \\ \colorbox{yellow} 0 & \colorbox{yellow} 0 & 8 & 9 \\ \colorbox{yellow} 0 & \colorbox{yellow} 0 & \colorbox{yellow} 0 & 11 \end{array} \right)\end{equation*}
$$

La matrice $B(4,4)$ est triangulaire inférieure car les coefficients au-dessus de la diagonale sont nuls :

$$
\begin{equation*}\mathbf{B} = \left(\begin{array}{cccc} 0 & \colorbox{yellow} 0 & \colorbox{yellow} 0 & \colorbox{yellow} 0  \\-1 & 4 & \colorbox{yellow} 0 & \colorbox{yellow} 0 \\ -2 & 5 & 8 & \colorbox{yellow} 0 \\ -6 & 2 & 9 & 11 \end{array} \right)\end{equation*}
$$
````

```{danger}
Ce n'est pas parce que les termes en-dessous ou au-dessous de la diagonale sont nuls que les autres ne le sont pas.

La matrice C est triangulaire supérieure :

$$
\begin{equation*}\mathbf{C} = \left(\begin{array}{cccc} 0 & 0 & 0 & 0  \\0 & 0 & \colorbox{cyan} {-1} & 0 \\ 0 & 0 & 0 & 0 \\ 0 & 0 & 0 & 0 \end{array} \right)\end{equation*}
$$
```

````{prf:definition}
:label: matrice-diagonale
 
**Matrice diagonale**:

Une matrice est diagonale si les tous les termes de part et d'autre de la diagonale sont nuls.
````

````{prf:example}

La matrice $D(4,4)$ est diagonale :

$$
\begin{equation*}\mathbf{D} = \left(\begin{array}{cccc} 3 & \colorbox{yellow} 0 & \colorbox{yellow} 0 & \colorbox{yellow} 0  \\\colorbox{yellow} 0 & 4 & \colorbox{yellow} 0 & \colorbox{yellow} 0 \\ \colorbox{yellow} 0 & \colorbox{yellow} 0 & 8 & \colorbox{yellow} 0 \\ \colorbox{yellow} 0 & \colorbox{yellow} 0 & \colorbox{yellow} 0 & 11 \end{array} \right)\end{equation*}
$$
````

```{danger}
Ce n'est pas parce que les termes en-dessous et au-dessous de la diagonale sont nuls que les autres ne le sont pas.

La matrice D est diagonale :

$$
\begin{equation*}\mathbf{D} = \left(\begin{array}{cccc} 3 & \colorbox{yellow} 0 & \colorbox{yellow} 0 & \colorbox{yellow} 0  \\\colorbox{yellow} 0 & \colorbox{green} 0 & \colorbox{yellow} 0 & \colorbox{yellow} 0 \\ \colorbox{yellow} 0 & \colorbox{yellow} 0 & \colorbox{green} 0 & \colorbox{yellow} 0 \\ \colorbox{yellow} 0 & \colorbox{yellow} 0 & \colorbox{yellow} 0 & 11 \end{array} \right)\end{equation*}
$$
```

````{note}
Une matrice diagonale est à la fois triangulaire supérieure et inférieure.
````


````{prf:definition}
:label: matrice-identite
 
**Matrice identité**:

La matrice identité est la matrice diagonale dont tous les éléments diagonaux sont égaux à 1.
````

````{prf:example}

Les matrices identité de dimension 3 et 4.

$$
\begin{equation*}\mathbf{I_3} = \left(\begin{array}{ccc} 1 & 0 & 0   \\0 & 1 & 0  \\ 0 & 0 & 1  \end{array} \right)\end{equation*}
$$

$$
\begin{equation*}\mathbf{I_4} = \left(\begin{array}{cccc} 1 & 0 & 0 & 0  \\0 & 1 & 0 & 0 \\ 0 & 0 & 1 & 0 \\ 0 & 0 & 0 & 1 \end{array} \right)\end{equation*}
$$

et plus généralement :

$$
\begin{equation*}\mathbf{I_n} = \left(\begin{array}{ccccc} 1 & 0 & \cdots & 0 & 0  \\ 0 & 1  & \cdots &  0 & 0 \\ \vdots & \vdots  & \ddots &  \vdots & \vdots \\ 0 & 0 & \cdots & 1 & 0 \\ 0 & 0 & \cdots & 0 & 1 \end{array} \right)\end{equation*}
$$
````




Sommes et produits de matrices
------------

````{prf:definition}
:label: matrice-somme
 
**Sommes de matrices**:

On ne peut additionner que des matrices ayant les mêmes dimensions et dans ce cas, c'est une addition terme à terme :

$$
A(n,p) + B(n,p) = S(n,p)
$$

$$
\forall (i,j) \: tel \: que \:1 \leq i \leq n \: et \: 1 \leq j \leq p, \: s_{i,j} = a_{i,j} + b_{i,j}
$$


````

````{prf:example}

Soient les 2 matrices $(4,2)$ $M$ et $P$:


$$
\begin{equation*}\mathbf{M} = \left(\begin{array}{ccc} 0 & \colorbox{yellow} 1  \\-1 & 2  \\-2 & 3  \\ \colorbox{green} {-5} & 7 \end{array} \right)\end{equation*}
$$

$$
\begin{equation*}\mathbf{P} = \left(\begin{array}{ccc} 8 & \colorbox{yellow} {10}  \\-10 & 20  \\-20 & 30  \\ \colorbox{green} {-50} & 70 \end{array} \right)\end{equation*}
$$

$$
\begin{equation*}\mathbf{M+P} = \left(\begin{array}{ccc} 8 & \colorbox{yellow} {11}  \\-11 & 22  \\-22 & 33  \\ \colorbox{green} {-55} & 77 \end{array} \right)\end{equation*}
$$
````

```{danger}
Si deux matrices n'ont pas exactement le même nombre de lignes et le même nombre de colonnes, on ne peut pas les additionner.
```


````{prf:definition}
:label: matrice-produit
 
**Produit de deux matrices**:
On peut effectuer le produit de deux matrices $M$ et $N$, dans cet ordre : $M*N$ à condition que le nombres de colonnes de $M$ soit égal au nombre de lignes de $N$.

C'est un produit ligne par colonne comme le montrent les exemples ci-dessous.
````

````{prf:example}

$(4,2) * (2,5) = (4,5)$


$M(4,2)$ est à gauche, en rose.

$N(2,5)$ est en haut, en vert.

$P(4,5)$ est à l'intersection, en jaune.

Pour trouver le terme $P_{2,4}$, c'est à dire 18, on multiplie la ligne 2 de $M$ par la colonne 4 de $N$.

Pour trouver le terme $P_{4,2}$, c'est à dire 15, on multiplie la ligne 4 de $M$ par la colonne 2 de $N$.

![matrice-produit-1](fig/ProduitMatrices.png)
````

````{prf:example}
$(6,3) * (3,4) = (6,4)$

$M(6,3)$ est à gauche, en rose.

$N(3,4)$ est en haut, en vert.

$P(6,4)$ est à l'intersection, en jaune.

Pour trouver le terme P2,3, c'est à dire 36, on multiplie la ligne 2 de $M$ par la colonne 3 de $N$.

Pour trouver le terme P6,2, c'est à dire 35, on multiplie la ligne 6 de $M$ par la colonne 2 de $N$.

![matrice-produit-2](fig/ProduitMatrices2.png)
````

```{danger}
La règle formelle de définition d'un produit de matrices est plus complexe que celle de l'addition terme à terme :

$$
\mathbf{A}(n,p) \cdot \mathbf{B}(p,q) = \mathbf{C}(n,q)
$$

$$
\forall (i,j) \:1 \leq i \leq n \: et \: 1 \leq j \leq q, \: \color{red} c_{i,j} = \sum _{k=1}^p a_{i,k} b_{k,j} \color{black}= a_{i,1}b_{1,j} + a_{i,2}b_{2,j} + \ldots + a_{i,p}b_{p,j}
$$

Les $a_{i,k}$ sont les $p$ éléments de la ligne $i$ de la matrice $A$.

Les $b_{k,j}$ sont les $p$ éléments de la colonne $j$ de la matrice $B$.
```

```{danger}
**Le produit des matrices n'est pas commutatif**

On n'a pas en général $\mathbf{A} \cdot \mathbf{B} = \mathbf{B} \cdot \mathbf{A}$ 

1. à cause des dimensions, $\mathbf{A} \cdot \mathbf{B}$ peut exister, mais pas forcément $\mathbf{B} \cdot \mathbf{A}$.
    Exemple:  $(6,3) * (3,2) = (6,2)$ mais $(3,2) * (6,3)$ n'existe pas.
1. quand les produits dans les deux sens existent, ils sont généralement de taille différentes.
    Exemple:  $(6,3) * (3,6) = (6,6)$ mais $(3,6) * (6,3) = (3,3)$
1. même quand les matrices sont carrées, et que les produits $\mathbf{A} \cdot \mathbf{B}$ et  $\mathbf{B} \cdot \mathbf{A}$ sont de même tailles, ils ont généralement des valeurs différentes (cf exemple).
```

`````{admonition} Produit de matrices carrées de même dimension.
:class: tip

Le produit de 2 matrices **carrées** de **même dimension** $(n,n)$ est **toujours possible :**

$$
\mathbf{A}(n,n) \cdot \mathbf{B}(n,n) = \mathbf{C}(n,n)
$$
`````

````{prf:example}
Les produits dans les deux sens de deux matrices $(3,3)$.

Ils existent mais ils sont différents.

En vert la matrice $A$, en jaune, la matrice $B$.

En orange, $\mathbf{A} \cdot \mathbf{B}$ et en rose, $\mathbf{B} \cdot \mathbf{A}$

On constate bien que:

$$
\mathbf{A} \cdot \mathbf{B} \ne \mathbf{B} \cdot \mathbf{A}
$$

![matrice-produit-2](fig/ProduitMatricesPasCom.png)
````

`````{admonition} Rôle de la matrice identité
:class: tip

$I_n$ est la matrice carrée $(n,n)$ diagonale dont tous les éléments diagonaux sont égaux à $1$.

$$
\begin{equation*}\mathbf{I_n} = \left(\begin{array}{ccccc} 1 & 0 & \cdots & 0 & 0  \\ 0 & 1  & \cdots &  0 & 0 \\ \vdots & \vdots  & \ddots &  \vdots & \vdots \\ 0 & 0 & \cdots & 1 & 0 \\ 0 & 0 & \cdots & 0 & 1 \end{array} \right)\end{equation*}
$$

**A savoir :**

Pour toute matrice carrée $A(n,n)$ :

$$
\color{blue} \mathbf{A} \cdot \mathbf{I_n} = \mathbf{I_n} \cdot \mathbf{A} = \mathbf{A}
$$

On dit que la matrice $I_n$ est l'élément neutre du produit des matrices carrées de dimension $n$.
`````

````{prf:example}

Les produits dans les deux sens d'une matrice carrée $A$ de dimension 3 par $I_3$.

Tous les deux redonnent $A$.

En vert la matrice $I_3$, en jaune, la matrice $A$.

En orange, $I_3 \cdot A$ et en rose, $A \cdot I_3$

On constate bien que:

$$
I_3 \cdot A = A \cdot I_3 = A
$$

![matrice-produit-2](fig/ProduitParId.png)
````

### Application: rotation


````{prf:theorem}

Soit la rotation de centre $O$ et d'angle $\phi$, qui transforme le vecteur $\vec{OM}$ en $\vec{OM'}$.
En notant $(x,y)$ les coordonnées de $M$, alors les coordonnées de $M'$ sont
obtenues par la multiplication matricielle: 

$$
\begin{equation*}
 \left(
    \begin{array}{c} 
    x' \\
    y' \\
       \end{array} 
\right)
=
 \left(
    \begin{array}{cc} 
    \cos\phi &  -\sin\phi\\
     \sin\phi & \cos\phi\\
       \end{array} 
\right)
\left(
    \begin{array}{c} 
    x \\
    y \\
       \end{array} 
    \right)    
\end{equation*}
$$

![rotation](fig/rotation.svg)

````
````{prf:proof}

Pour prouver cette propriété on écrit les $(x,y)$ en coordonnées polaires:

$$
\begin{eqnarray}
x &=& r \cos \theta \nonumber \\
y &=& r \sin \theta \nonumber 
\end{eqnarray}
$$

De même pour les coordonnées du vecteur après rotation:

$$
\begin{eqnarray}
x' &=& r \cos( \theta +\phi) \nonumber \\
y' &=& r \sin( \theta +\phi) \nonumber 
\end{eqnarray}
$$

En appliquant les formules trigonométriques, on a:

$$
\begin{eqnarray}
\cos(a+b) &=&  \cos a \cos b- \sin a \sin b \nonumber \\
\sin(a+b) &=& \sin a \cos b + \cos a \sin b \nonumber \\
x' &=& r (  \cos \theta \cos \phi- \sin \theta \sin \phi  )  \nonumber \\
   &=& x \cos \phi - y  \sin \phi  \nonumber \\
y' &=& r( \sin \theta \cos \phi + \cos \theta \sin \phi)  \nonumber \\
    &=& y \cos \phi + x \sin \phi
\end{eqnarray}
$$

Ce qui peut s'écrire comme un produit matriciel:

$$
\begin{equation*}
 \left(
    \begin{array}{c} 
    x' \\
    y' \\
       \end{array} 
\right)
=
 \left(
    \begin{array}{cc} 
    \cos\phi &  -\sin\phi\\
     \sin\phi & \cos\phi\\
       \end{array} 
\right)
\left(
    \begin{array}{c} 
    x \\
    y \\
       \end{array} 
    \right)    
\end{equation*}
$$
````


```{admonition} Cas complexe

On peut retrouver cette formule en remplaçant le point $M$ en dimension 2 par un complexe $z$.
La rotation d'un angle $\phi$ s'écrit:

$$
\begin{eqnarray}
z' &=& e^{i \phi}z \nonumber 
\end{eqnarray}
$$

En effet, en posant $z= x+iy$ et  $z'= x'+iy'$, on a 

$$
\begin{eqnarray}
x'+i y' &=& (\cos \phi + i \sin \phi) (x+i y) \nonumber 
\end{eqnarray}
$$

En identifiant la partie réelle d'un côté et la partie imaginaire de l'autre, on obtient: 

$$
\begin{eqnarray}
x'  &=& x \cos \phi - y \sin \phi   \nonumber \\
y' &=& x \sin \phi + y\cos \phi
\end{eqnarray}
$$

Et on retrouve la formule vue plus haut.
```

### Application: produit scalaire

````{prf:definition}
:label: produit-scalaire

Le **produit scalaire** de deux vecteurs $\vec{OA},\vec{OB}$ est le projeté orthogonal de $\vec{OA}$
sur la droite $(OB)$. 
On le note $\vec{OA}.\vec{OB}$. C'est un  **nombre**, dont la valeur peut se calculer de **plusieurs manières**.

![prod-scal](fig/scalar_product.svg)
Source: wikipedia
````

````{prf:property}
La valeur du produit scalaire $\vec{OA}.\vec{OB}$ est $OA\times OB \times \cos \theta$.

NB: il faut connaître l'angle, ce qui n'est pas toujours facile (e.g. en dimension $>2$).
````


````{prf:property}
Le produit scalaire de deux vecteurs orthogonaux vaut $0$.
````

````{prf:property}
Soient deux vecteurs $\vec{x},\vec{y}$. Leurs
coordonnées dans une base $B$ sont des **vecteurs**: $\vec{x}  \leftrightarrow X  $, $\vec{y} \leftrightarrow Y $.

Par exemple en dimension $2$:

$$
\begin{equation*}
X=
 \left(
    \begin{array}{c} 
    x_1 \\
    x_2 \\
       \end{array} 
\right),
~Y=
 \left(
    \begin{array}{c} 
    y_1 \\
    y_2 \\
       \end{array} 
\right)
\end{equation*}
$$

Alors une autre formule pour calculer la valeur du produit scalaire  :

$$
\begin{equation*}
\vec{x}.\vec{y}  = ^tXY = ^tYX = x_1 y_1 + x_2 y_2
\end{equation*}
$$
````
````{prf:property}
On peut écrire la même chose en dimension $3,4,\ldots$.
Par exemple en dimension $3$, on aura:

$$
\begin{equation*}
\vec{x}.\vec{y}  = x_1 y_1 + x_2 y_2+x_3 y_3
\end{equation*}
$$
````

```{admonition} Quiz

Un quiz sur les définitions de base des matrices, sur [Eprel](https://eprel.u-pec.fr/mod/quiz/view.php?id=166507)

```


Déterminant d'une matrice carrée.
------------

Dans le paragraphe suivant, nous verrons la notion de matrice inverse.

Sans entrer dans le détail, l'inverse d'une matrice carrée $A$ est la matrice $A^{-1}$ telle que $A \cdot A^{-1} = A^{-1} \cdot A = I$ où $I$ est la matrice identité de même dimension que $A$.

Mais toute matrice carrée n'est pas inversible, c'est à dire que $A^{−1}$ n'existe pas toujours. C'est le déterminant de $A$ qui va indiquer (déterminer) si $A$ est inversible.

Plus précisément : Une matrice A est inversible $\color{blue} \mathbf {\Leftrightarrow det(\mathbf{A}) \ne 0}$.

Ce paragraphe vous indique comment calculer successivement :

* un déterminant (1,1). 
* un déterminant (2,2). 
* un déterminant (3,3).

Ceux qui veulent verront ici comment calculer un déterminant $(n,n)$ même si les déterminants $(3,3)$ sont suffisants en GEII.


### Calcul des déterminants (1,1)

`````{admonition} Calcul pratique d'un déterminant (1,1)
:class: tip

Soit la matrice (1,1):

$$
\mathbf{A} = \left(\begin{array}{c} a 
                        \end{array} \right)
$$

alors le déterminant de A, noté  $det(A)$ ou $\left| a \right|$ vaut $a$.
`````

````{prf:example}
Soit la matrice (1,1):

$$
\mathbf{A} = \left(\begin{array}{c} 5 \end{array} \right)
$$

Alors :

$$
det(A)=\left|\begin{array}{c} 5 \end{array} \right| = 5
$$

````

### Calcul des déterminants (2,2)

`````{admonition} Calcul pratique d'un déterminant (2,2)
:class: tip

Soit la matrice:

$$
\mathbf{A} = \left(\begin{array}{cc} a & b  \\ c & d  \end{array} \right)
$$

alors le déterminant de A, noté  $det(A)$ ou $\left|\begin{array}{cc} a & b  \\ c & d  \end{array} \right|$ vaut
 
$$
det(A)=\left|\begin{array}{cc} a & b  \\ c & d  \end{array} \right| = ad-bc
$$
`````

````{prf:example}
Soit la matrice :

$$
\mathbf{A} = \left(\begin{array}{cc} 5 & 3  \\ 2 & 4  \end{array} \right)
$$

Alors :

$$
det(A)=\left|\begin{array}{cc} 5 & 3  \\ 2 & 4  \end{array} \right| = 5*4-2*3=20-6=14
$$

![matrice-det-2x2](fig/Det2x2.png)
````

```{danger}
Attention aux signes !

$$
\mathbf{A} = \left(\begin{array}{cc} 5 & -3  \\ 2 & -4  \end{array} \right)
$$

$$
det(A)=\left|\begin{array}{cc} 5 & -3  \\ 2 & -4  \end{array} \right| = 5*(-4)-2*(-3)=-20-(-6)=-20+6=-14
$$
```

````{note}
Le déterminant de la matrice A n'est pas nul, donc elle est inversible.
````

### Calcul des déterminants (3,3)

Un déterminant $(3,3)$ se calcule en combinant 3 déterminants $(2,2)$:

De même:

* qu'un déterminant $(4,4)$ se calcule en combinant 4 déterminants $(3,3)$.
* qu'un déterminant $(5,5)$ se calcule en combinant 5 déterminants $(4,4)$
* ...

Reste à comprendre ce qu'on entend par "combiner".


`````{admonition} Calcul pratique d'un déterminant $(3,3)$ par développement selon une ligne ou une colonne.
:class: tip

Expliquons sur un exemple :

$$
\mathbf{A} = \left(\begin{array}{ccc} 5 & -3 & -6  \\ 2 & -4 & 7 \\ -1 & -9 & 8  \end{array} \right)
$$

**Étape 1**

On affecte chaque nombre de la matrice d'un signe qui va être utile pendant le développement.

Le premier coefficient est affecté du signe + et ensuite on alterne, qu'on se déplace horizontalement ou verticalement.

$$
\mathbf{A} = \left(\begin{array}{ccc} 5 \color{red} {^+} & -3 \color{red} ^- & -6 \color{red} ^+  \\ 2 \color{red} ^- & -4 \color{red} ^+& 7 \color{red} ^- \\ -1 \color{red} ^+ & -9 \color{red} ^- & 8\color{red} ^+  \end{array} \right)
$$

**Étape 2**

On choisit la ligne ou la colonne selon laquelle on va développer. On a donc 6 choix.

Ici on va développer selon la première ligne.

$$
\det(\mathbf{A}) = \color{red} + \color{black} 5\left|\begin{array}{cc} -4 & 7  \\ -9 & 8  \end{array} \right| \color{red} - \color{black} (-3)\left|\begin{array}{cc}2 & 7  \\ -1 & 8  \end{array} \right| \color{red} +\color{black} (-6)\left|\begin{array}{cc} 2 & -4  \\ -1 & -9  \end{array} \right|
$$

* Explications :
   * les nombres de la première ligne 5, (-3) et (-6) sont précédés des signes qui leur ont été affectés dans l'étape 1 ;
   * Chaque nombre de la première ligne est multiplié par le déterminant (2,2) obtenu en cachant la ligne et la colonne de ce nombre là. Ainsi :
        * 5 est multiplié par le déterminant (2,2) qui reste quand on barre la première ligne et la première colonne de A : 
           $\left(\begin{array}{ccc} 5 & \colorbox{black} {-3} & \colorbox{black} {-6}  \\ \colorbox{black} {-2} & \color{blue} \textbf {-4} & \color{blue} \textbf 7 \\ \colorbox{black} {-1} & \color{blue} \textbf {-9} & \color{blue} \textbf 8  \end{array} \right)$
        * -3 est multiplié par le déterminant (2,2) qui reste quand on barre la première ligne et la deuxième colonne de A :
           $\left(\begin{array}{ccc} \colorbox{black} {-5} & -3 & \colorbox{black} {-6}  \\ \color{blue} \textbf {2} & \colorbox{black} {-4} & \color{blue} \textbf 7 \\ \color{blue} \textbf {-1} & \colorbox{black} {-9} & \color{blue} \textbf 8  \end{array} \right)$
        * -6 est multiplié par le déterminant (2,2) qui reste quand on barre la première ligne et la troisième colonne de A :
           $\left(\begin{array}{ccc} \colorbox{black} {-5} & \colorbox{black} {-3} & -6  \\ \color{blue} \textbf {2} & \color{blue} \textbf {-4} & \colorbox{black} {-7} \\ \color{blue} \textbf {-1} & \color{blue} \textbf {-9} & \colorbox{black} {-8}  \end{array} \right)$
           
**Étape 3**

Il ne reste plus qu'à calculer les 3 déterminants (2,2) :

$$
\begin{align}\det(\mathbf{A}) & = \color{red} + \color{black} 5\left|\begin{array}{cc} -4 & 7  \\ -9 & 8  \end{array} \right| \color{red} - \color{black} (-3)\left|\begin{array}{cc}2 & 7  \\ -1 & 8  \end{array} \right| \color{red} +\color{black} (-6)\left|\begin{array}{cc} 2 & -4  \\ -1 & -9  \end{array} \right| \\ & = \textbf 5\left(-4*8-(-9)*7\right) + \textbf 3\left(2*8-(-1)*7\right) \textbf {-6}\left(2*(-9)-(-1)*(-4)\right) \\ & = 5*31 + 3*23 -6*-(22) \\ & = 155 + 69 + 132 \\ & = 356\end{align}
$$
`````

````{note}
Si on avait développé le déterminant de A selon la dernière colonne, par exemple, on aurait trouvé :

$$
\begin{align}\det(\mathbf{A}) & = \color{red} +\color{black} (-6)\left|\begin{array}{cc} 2 & -4  \\ -1 & -9  \end{array} \right| \color{red} -\color{black} 7\left|\begin{array}{cc} 5 & -3  \\ -1 & -9  \end{array} \right| \color{red} +\color{black} 8\left|\begin{array}{cc} 5 & -3  \\ 2 & -4  \end{array} \right| \\ & = \textbf {-6}\left(2*(-9)-(-1)*(-4)\right) \textbf {-7}\left(5*(-9)-(-1)*(-3)\right) \textbf {+8}\left(5*(-4)-2*(-3)\right)\\ & = -6*(-22) -7*(-48) +8*(-14) \\ & = 132 + 336-112 \\ & = 356\end{align}
$$

Et on trouve la même chose, ce qui est rassurant ;-). On trouverait 356 quelle que soit la ligne ou la colonne selon laquelle on aurait choisi de développer.

Là encore, le déterminant de la matrice A est non nul, donc elle est inversible.
````

`````{admonition} Conseil: utiliser les lignes ou les colonnes qui contiennent des 0
:class: tip

Puisque les déterminants (2,2) sont multipliés par les coefficients d'une ligne ou d'une colonne, on a intérêt à développer selon la ligne ou la colonne qui contient le plus de 0. Ceci évite le calcul des sous-déterminants.
`````

````{prf:example}

Puisque les déterminants (2,2) sont multipliés par les coefficients d'une ligne ou d'une colonne, on a intérêt à développer selon la ligne ou la colonne qui contient le plus de 0. Ceci évite le calcul des sous-déterminants.

$$
\mathbf{A} = \left(\begin{array}{ccc} 5 & -3 & -6  \\ 2 & -4 & 7 \\ 0 & 0 & 8  \end{array} \right)
$$

On développe selon la dernière ligne :

$$
\begin{align}det(\mathbf{A}) &= 0\left|\begin{array}{cc} -3 & -6  \\ -4 & 7  \end{array} \right| - 0\left|\begin{array}{cc} 5 & -6  \\ 2 & 7  \end{array} \right|+8\left|\begin{array}{cc} 5 & -3  \\ 2 & -4  \end{array} \right| \\ &=0-0+ 8\left(5*(-4)-2*(-3)\right) \\ &= 8*(-14) = -112 \end{align}
$$
````

`````{admonition} La règle de Sarrus
:class: tip

La règle de Sarrus est un autre moyen pratique de calculer un déterminant (3,3).

Explications en vidéos [youtube](https://youtu.be/geJKlKL0z68)
`````

````{prf:example}
La règle de Sarrus sur un exemple.

![Sarrus-ex](fig/Sarrus.png)
````
<h3> Mineur et cofacteur </h3>

### Mineur et cofacteur


````{prf:definition}
:label: matrice-mineur
 
**Mineur**:
Le mineur d'un terme $\alpha$ d'une matrice est le déterminant obtenu lorsqu'on barre la ligne et la colonne de $\alpha$.

Ce sont les sous-déterminants qui apparaissent lorsqu'on développe un déterminant selon une ligne ou une colonne.
````

````{prf:example}

$$
\mathbf{A} = \left(\begin{array}{ccc} 5 & -3 & -6  \\ 2 & -4 & 7 \\ -1 & -9 & 8  \end{array} \right)
$$

* Le mineur de 5 est $\left|\begin{array}{cc} -4 & 7  \\ -9 & 8  \end{array} \right|$
* Le mineur de -3 est $\left|\begin{array}{cc}2 & 7  \\ -1 & 8  \end{array} \right|$

````

````{prf:definition}
:label: matrice-cofacteur
 
**Cofacteur**:

Le cofacteur d'un terme $\alpha$ d'une matrice est le mineur de $\alpha$ précédé du signe +/− qu'on a affecté à $\alpha$ pour calculer le déterminant de la matrice.
````

````{prf:example}

$$
\mathbf{A} = \left(\begin{array}{ccc} 5 \color{red} {^+} & -3 \color{red} ^- & -6 \color{red} ^+  \\ 2 \color{red} ^- & -4 \color{red} ^+& 7 \color{red} ^- \\ -1 \color{red} ^+ & -9 \color{red} ^- & 8\color{red} ^+  \end{array} \right)
$$

* Le cofacteur de 5 est $\color{red} {^+} \color{black} \left|\begin{array}{cc} -4 & 7  \\ -9 & 8  \end{array} \right|$
* Le cofacteur de -3 est $\color{red} {^-} \color{black} \left|\begin{array}{cc}2 & 7  \\ -1 & 8  \end{array} \right|$
* Le cofacteur de 7 est $\color{red} {^-} \color{black} \left|\begin{array}{cc} 5 & -3  \\ -1 & -9  \end{array} \right|$

````


```{admonition} Quiz

Un quiz sur les déterminants, sur [Eprel](https://eprel.u-pec.fr/mod/quiz/view.php?id=171180)

```


Matrice inverse
-------------

````{prf:definition}
:label: matrice-inverse
 
**Inverse d'une matrice**:

On dit qu'une matrice carrée $\mathbf{A}$ est inversible, si on peut trouver:

*   une matrice $\mathbf{P}$ telle que $\mathbf{A} \cdot \mathbf{P} = \mathbf{I_n}$ (où $ \mathbf{I_n}$ est la matrice identité de même dimension que $\mathbf{A}$ ) ;
*   ou une matrice $\mathbf{Q}$ telle que $\mathbf{Q} \cdot \mathbf{A} = \mathbf{I_n}$

Dans ce cas, on montre que $\mathbf{P}$ et $\mathbf{Q}$  sont égales et on note $\mathbf{A^{-1}}$  l'inverse de la matrice $\mathbf{A}$.

$\mathbf{A^{-1}}$  est donc l'unique matrice telle que $\mathbf{A} \cdot \mathbf{A^{-1}} = \mathbf{A^{-1}} \cdot \mathbf{A} = \mathbf{I_n}$

````

```{note}
**Analogie avec les réels**

Tout réel non nul $x \in \mathbb{R}^*$ est inversible, tel que :

* $x \cdot \frac{1}{x}=1$
* $ \frac{1}{x} \cdot x=1$

Le seul contre-exemple est $x=0$

Pour les matrices, en revanche, de nombreuses matrices sont non-inversibles (cf ci-dessous).
```

```{danger}
Seules les matrices carrées peuvent être inversibles. Dans ce paragraphe, toutes les matrices sont carrées !
```


````{prf:example}
**Une matrice (2,2) inversible**

Soit:

$$
\mathbf{M} = \left(\begin{array}{cc} 1 & 2  \\ 1 & 3  \end{array} \right)
$$

M est inversible et $\mathbf{M^{-1}} = \left(\begin{array}{cc} 3 & -2  \\ -1 & 1  \end{array} \right)$, en effet:

$$
\mathbf{M} \cdot \mathbf{M^{-1}} = \left(\begin{array}{cc} 1 & 2  \\ 1 & 3  \end{array} \right) \cdot \left(\begin{array}{cc} 3 & -2  \\ -1 & 1  \end{array} \right) = \left(\begin{array}{cc} 1 & 0  \\ 0 & 1  \end{array} \right) = \mathbf{I_2}
$$

Et:

$$
\mathbf{M^{-1}} \cdot \mathbf{M} = \left(\begin{array}{cc} 3 & -2  \\ -1 & 1  \end{array} \right) \cdot \left(\begin{array}{cc} 1 & 2  \\ 1 & 3  \end{array} \right) = \left(\begin{array}{cc} 1 & 0  \\ 0 & 1  \end{array} \right) = \mathbf{I_2}
$$
````

```{danger}
**Une matrice (2,2) non-inversible**

Certaines matrices ne sont pas inversibles :

Soit:

$$
\mathbf{P} = \left(\begin{array}{cc} 2 & 1  \\ 0 & 0  \end{array} \right)
$$

En raisonnant **par l'absurde**, si on cherche $\mathbf{P^{-1}} = \left(\begin{array}{cc} a & c  \\ b & d  \end{array} \right)$  telle que :

$$
\mathbf{P^{-1}} \cdot \mathbf{P} = \left(\begin{array}{cc} a & c  \\ b & d  \end{array} \right) \cdot \left(\begin{array}{cc} 2 & 1  \\ 0 & 0  \end{array} \right) = \left(\begin{array}{cc} 1 & 0  \\ 0 & 1  \end{array} \right) = \mathbf{I_2}
$$

alors on obtient :

$$
\left(\begin{array}{cc} 2a & a  \\ 2b & b  \end{array} \right) = \left(\begin{array}{cc} 1 & 0  \\ 0 & 1  \end{array} \right)
$$

On devrait donc avoir en comparant les premières lignes : $2a = 1$ et $a = 0$, ce qui est impossible.

La matrice $\mathbf{P} = \left(\begin{array}{cc} 2 & 1  \\ 0 & 0  \end{array} \right)$  n'est pas inversible. 

$\mathbf{P^{-1}}$ n'existe pas.
```

```{note}
Ces deux exemples montrent que certaines matrices sont inversibles et d'autres non. On va voir dans la suite de cette page comment :

*  savoir si une matrice est inversible ;
*  calculer son inverse le cas échéant.
```

```{important}
Une matrice A est inversible $ \color{blue} \mathbf {\Leftrightarrow det(\mathbf{A}) \ne 0}$

Ce théorème est admis en GEII.
```

````{prf:example}
On a vu ci-dessus que $\mathbf{P} = \left(\begin{array}{cc} 1 & 2  \\ 1 & 3  \end{array} \right)$  était inversible.

Son déterminant vaut :

$$
det(\mathbf{M}) = \left|\begin{array}{cc} 1 & 2  \\ 1 & 3  \end{array} \right|= 1*3-1*2 = 1
$$

Il est bien différent de $0$.
````


````{prf:example}
On a vu ci-dessus que $\mathbf{P} = \left(\begin{array}{cc} 2 & 1  \\ 0 & 0  \end{array} \right)$  n'était pas inversible.

Son déterminant vaut :

$$
det(\mathbf{P}) = \left|\begin{array}{cc} 2 & 1  \\ 0 & 0  \end{array} \right|= 2*0 - 0*1 = 0
$$

Il est bien nul.
````


````{prf:definition}
:label: matrice-comatrice
 
**Comatrice**:

La comatrice (ou matrice adjointe) d'une matrice $\mathbf{A}$ est la matrice des cofacteurs de $\mathbf{A}$.
````

````{prf:example}
**En dimension 2**:

Soit: $\mathbf{A} = \left(\begin{array}{cc} a_{1,1} & a_{1,2}  \\ a_{2,1} & a_{2,2}   \end{array} \right)$

Alors :

$$
com(\mathbf{A}) = \left(\begin{array}{cc} cof(a_{1,1}) & cof(a_{1,2})  \\ cof(a_{2,1}) &cof(a_{2,2})   \end{array} \right)
$$

où on a noté $cof(a_{i,j})$ le cofacteur du terme $a_{i,j}$ dans la matrice $\mathbf{A}$

````


```{note}
**Rappel: Cofacteur**:
on a vu en {prf:ref}`matrice-cofacteur` que le cofacteur d'un terme $\alpha$ d'une matrice est le mineur de $\alpha$ précédé du signe +/− qu'on a affecté à $\alpha$ pour calculer le déterminant de la matrice.
```

````{prf:example}
**En dimension 2**:

Soit:

$\mathbf{A} = \left(\begin{array}{cc} 0 & 1  \\ 4 & 0   \end{array} \right)$

Le cofacteur du terme $a_{1,1}$ vaut:

$$
cof(a_{1,1}) =
\textcolor{red}{+} 
\left|\begin{array}{cc} 
\colorbox{black} {0}    & \colorbox{black} {1}   \\ 
  \colorbox{black} {4}  & 0 
\end{array} \right|
= det([0])=0
$$

On fait de même pour tous les $(i,j)$ et on obtient: 

$$
cof(a_{1,2}) =
\textcolor{red}{-} 
\left|\begin{array}{cc} 
\colorbox{black} {0}    & \colorbox{black} {1}   \\ 
  4  & \colorbox{black}{0} 
\end{array} \right| = -det([4])=-4
$$

$$
cof(a_{2,1}) =
\textcolor{red}{-} 
\left|\begin{array}{cc} 
\colorbox{black} {0}    & 1   \\ 
\colorbox{black} {4}  & \colorbox{black}{0} 
\end{array} \right| = -det([1])=-1
$$

$$
cof(a_{2,2}) =
\textcolor{red}{+} 
\left|\begin{array}{cc} 
0    & \colorbox{black} {1}   \\ 
\colorbox{black} {4}  & \colorbox{black}{0} 
\end{array} \right| = det([0])=0
$$

Finalement:

$$
com(\mathbf{A}) =
\left(\begin{array}{cc} 
   0 & -4  \\ 
  -1 & 0
\end{array} \right)
$$
````

````{prf:example}
**En dimension 3**:

Soit:

$$
\mathbf{A} = \left(\begin{array}{ccc} 0 & 1 & 3   \\ 4 & 0 & -3 \\ -2 & 1 & 4\end{array} \right)
$$

Le cofacteur du terme $a_{1,1}$ vaut:

$$
cof(a_{1,1}) =
\textcolor{red}{+} 
\left|\begin{array}{ccc} 
\colorbox{black} {0} & \colorbox{black} {1} & \colorbox{black} {3}  \\
\colorbox{black} {4} & 0 & -3 \\ 
\colorbox{black} {-2} & 1 & 4
\end{array} \right|
= 
\left|\begin{array}{cc} 
 0 & -3 \\ 
 1 & 4
\end{array} \right|
=0*4 - 1.(-3) = 3
$$

On commence donc à remplir le terme $(1,1)$ de la comatrice avec la valeur $3$:

$$
\mathbf{com(A)} = \left(\begin{array}{ccc} 3 &  &    \\ &  &  \\  & &  \end{array} \right)
$$

On fait de même pour tous les $(i,j)$ et on obtient finalement:

$$
\mathbf{com(A)} = \left(\begin{array}{ccc} 3 & -10 & 4   \\ -1 & 6 & -2 \\ -3 & 12 & -4\end{array} \right)
$$
````

````{prf:theorem}
:label: matrice-comatrice-product

Soit A une matrice carrée de dimension n :

$$
\mathbf{A} \cdot \mathbf{^t com(A)} = \mathbf{^t com(A)} \cdot \mathbf{A} = det(\mathbf{A} )*\mathbf{I_n} 
$$
````

````{prf:example}

Soit :

$$
\mathbf{A} = \left(\begin{array}{ccc} 0 & 1 & 3   \\ 4 & 0 & -3 \\ -2 & 1 & 4\end{array} \right)
$$

Son déterminant développé selon la première ligne vaut:

$$
-1\left|\begin{array}{ccc} 4 & -3  \\ -2 & 4\end{array} \right|+3\left|\begin{array}{ccc} 4 & 0  \\ -2 & 1\end{array} \right|=-10+12=2
$$

On a vu dans l'exemple précédent que sa comatrice valait:

$$
\mathbf{com(A)} = \left(\begin{array}{ccc} 3 & -10 & 4   \\ -1 & 6 & -2 \\ -3 & 12 & -4\end{array} \right)
$$

La transposée de sa comatrice vaut :

$$
\mathbf{^t com(A)} = \left(\begin{array}{ccc} 3 & -1 & -3   \\ -10 & 6 & 12 \\ 4 & -2 & -4\end{array} \right)
$$

On vérifie bien que :

$$
\mathbf{A} \cdot \mathbf{^t com(A)} = \left(\begin{array}{ccc} 0 & 1 & 3   \\ 4 & 0 & -3 \\ -2 & 1 & 4\end{array} \right) \cdot \left(\begin{array}{ccc} 3 & -1 & -3   \\ -10 & 6 & 12 \\ 4 & -2 & -4\end{array} \right) = \left(\begin{array}{ccc} 2 & 0 & 0   \\ 0 & 2 & 0 \\ 0 & 0 & 2\end{array} \right)
$$

Et que:

$$
\mathbf{^t com(A)} \cdot \mathbf{A} =  \left(\begin{array}{ccc} 3 & -1 & -3   \\ -10 & 6 & 12 \\ 4 & -2 & -4\end{array} \right) \cdot \left(\begin{array}{ccc} 0 & 1 & 3   \\ 4 & 0 & -3 \\ -2 & 1 & 4\end{array} \right) = \left(\begin{array}{ccc} 2 & 0 & 0   \\ 0 & 2 & 0 \\ 0 & 0 & 2\end{array} \right)
$$

Ces deux derniers termes sont bien égaux à $det(A) \cdot I_3 = 2 I_3$

````

```{important}
Lorsque le déterminant d'une matrice carrée A n'est pas nul, la formule précédente fournit l'inverse de A.

$$
\color{blue} \textrm{Si} \: \det(\mathbf{A} ) \ne 0 \: \textrm{alors} \: \mathbf{A^{-1}}= \frac{1}{\det(\mathbf{A} )}\mathbf{^t com(A)}
$$
```

````{prf:proof}

D'abord on ré-écrit {prf:ref}`matrice-comatrice-product`. Puis on divise des deux côtés par $det(A)$, qui est supposé non nul:

$$
\frac{1}{\det(\mathbf{A} )} \mathbf{A} \cdot \mathbf{^t com(A)} =\frac{1}{\det(\mathbf{A} )} \mathbf{^t com(A)} \cdot \mathbf{A} = \mathbf{I_n} 
$$

On garde juste le membre de gauche:

$$
\mathbf{A} \cdot \frac{1}{\det(\mathbf{A} )}\mathbf{^t com(A)} = \mathbf{I_n} 
$$

On multiplie des deux côtés par $\mathbf{A^{-1}}$, puisque cette matrice existe

$$
\mathbf{A^{-1}} \mathbf{A} \cdot \frac{1}{\det(\mathbf{A} )}\mathbf{^t com(A)} = \mathbf{A^{-1}} \mathbf{I_n} 
$$

Comme $\mathbf{A^{-1}} \mathbf{A}= \mathbf{I_n}$ et $\mathbf{A^{-1}} \mathbf{I_n}=\mathbf{A^{-1}}$, on en déduit:

$$
\mathbf{A^{-1}}= \frac{1}{\det(\mathbf{A} )}\mathbf{^t com(A)}
$$

````


````{prf:example}
Continuons avec :

$$
\mathbf{A} = \left(\begin{array}{ccc} 0 & 1 & 3   \\ 4 & 0 & -3 \\ -2 & 1 & 4\end{array} \right)
$$

La transposée de sa comatrice vaut:

$$
\mathbf{^t com(A)} = \left(\begin{array}{ccc} 3 & -1 & -3   \\ -10 & 6 & 12 \\ 4 & -2 & -4\end{array} \right)
$$

Donc son inverse :

$$
\mathbf{A^{-1}}= \frac{1}{\det(\mathbf{A} )}\mathbf{^t com(A)} = \frac{1}{2} \cdot \left(\begin{array}{ccc} 3 & -1 & -3   \\ -10 & 6 & 12 \\ 4 & -2 & -4\end{array} \right) = \left(\begin{array}{ccc} 1.5 & -0.5 & -1.5   \\ -5 & 3 & 6 \\ 2 & -1 & -2\end{array} \right)
$$
````

```{danger}
La formule $\mathbf{A^{-1}}= \frac{1}{\det(\mathbf{A} )}\mathbf{^t com(A)}$ a un intérêt en dimension 2 et 3.

Au-dessus, les temps de calcul sont trop longs et on a recours à d'autres méthodes.
```

```{note}
**Formule de l'inverse en dimension 2**

L'application de la formule de l'inverse en dimension 2 est directe :

$$
\mathbf{A} = \left(\begin{array}{cc} a & c  \\ b & d  \end{array} \right)
$$

Si $det(\mathbf{A}) = ad-bc \ne 0$, alors:

$$
\mathbf{A^{-1}} = \frac{1}{ad-bc}\left(\begin{array}{cc} d & -c  \\ -b & a  \end{array} \right)
$$
```
````{prf:proof}
A faire en exercice: 

d'abord calculer le déterminant de $\mathbf{A}$ en fonction de $a,b,c,d$.

Puis calculer la transposée de la comatrice de $\mathbf{A}$.
````


````{prf:example}
Soit :

$$
\mathbf{A} = \left(\begin{array}{cc} 0 & 1 \\ 4 & 0\end{array} \right)
$$

Alors $\det(\mathbf{A}) = -4$

Donc:

$$
\mathbf{A}^{-1} = \frac{1}{-4} \left(\begin{array}{cc} 0 & -1 \\ -4 & 0\end{array} \right)
$$

````


```{admonition} Quiz

Un quiz sur les matrices inverses, sur [Eprel](https://eprel.u-pec.fr/mod/quiz/view.php?id=171181)

```


Approche matricielle des systèmes linéaires
--------------



````{prf:definition}
:label: matrice1
 
**Système linéaire**:

Un système linéaire est un ensemble de $n$ équations à $p$ inconnues.

$$
(S):\left\{\begin{array}{ccc}a_{11} x_1 + \ldots + a_{1p} x_p &=& b_1 \\\vdots &&\\a_{n1} x_1 + \ldots +a_{np} x_p &=& b_n \\\end{array} \right.
$$

En GEII, on s'intéressa quasi exclusivement aux systèmes "carrés" : même nombre d'équations que d'inconnues.
````

````{prf:example}
$$
(S) :\left\{\begin{array}{ccccccc} x & + & 2y & -& 3z &=& 8  \\ 2x & - & 2y & - & z &=& -1  \\ 3x & + & 2y & + & z &=& 6  \\\end{array} \right.
$$
````

```{note}
Un système linéaire peut être considéré comme une égalité entre :

* le produit de la matrice du système par le vecteur des inconnues ;
* le vecteur des constantes (le second membre)
```

````{prf:example}
$$
(S) :\left\{
    \begin{array}{ccccccc} x & + & 2y & -& 3z &=& 8  \\ 
    2x & - & 2y & - & z &=& -1  \\
    3x & + & 2y & + & z &=& 6  \\
    \end{array} \right. 
\Leftrightarrow 
 \underbrace {\left(\begin{array}{ccc} 1& 2 & -3 \\ 2 & -2 & -1 \\ 3 & 2 & 1 \\\end{array} \right)}_{\mathbf{A}} \cdot \underbrace {\left(\begin{array}{c} x \\ y \\ z \\\end{array} \right)}_{\mathbf{X}}= \underbrace {\left(\begin{array}{c} 8 \\ -1 \\ 6 \\\end{array} \right)}_{\mathbf{B}}
$$

Le système équivaut donc au produit : $\mathbf{A} \cdot \mathbf{X} = \mathbf{B}$ , dans lequel :

* $\mathbf{A} = \left(\begin{array}{ccc} 1& 2 & -3 \\ 2 & -2 & -1 \\ 3 & 2 & 1 \\\end{array} \right)$  est la matrice du système ;
* $\mathbf{X} = \left(\begin{array}{c} x \\ y \\ z \\\end{array} \right)$  est le vecteur inconnu ;
* $\mathbf{B} =\left(\begin{array}{c} 8 \\ -1 \\ 6 \\\end{array} \right)$  est le second membre.
````

```{important}
Soit un système linéaire $\mathbf{A} \cdot \mathbf{X} = \mathbf{B}$  de $n$ équations à $n$ inconnues dans lequel :

* $A$ est la matrice du système ;
* $X$ le vecteur **inconnu** ;
* $B$ le second membre.

Alors, si A est inversible $(\det(\mathbf{A}) \ne 0)$  , le système admet la solution **unique** $ \mathbf{X} = \mathbf{A^{-1}} \cdot \mathbf{B}$
```

````{prf:proof}
On part de : $\mathbf{A} \cdot \mathbf{X} = \mathbf{B}$

On multiplie des 2 côtés à gauche par $\mathbf{A^{-1}}$, ce qui donne : 

$$
\mathbf{A^{-1}} \cdot \mathbf{A} \cdot \mathbf{X} = \mathbf{A^{-1}} \cdot \mathbf{B}
$$

Or : $\mathbf{A^{-1}} \cdot \mathbf{A} = \mathbf{I}$

Donc: $\mathbf{I} \cdot \mathbf{X} = \mathbf{A^{-1}} \cdot \mathbf{B}$

Donc: $\mathbf{X} = \mathbf{A^{-1}} \cdot \mathbf{B}$

````

```{note}
**Analogie avec les réels**:

L'équivalence $\mathbf{A} \cdot \mathbf{X} = \mathbf{B} \Leftrightarrow \mathbf{X} = \mathbf{A^{-1}} \cdot \mathbf{B}$
est à rapprocher de ce qui se passe dans $\mathbb{R}$:

$$
(E): ~ax=b \Leftrightarrow x=\frac{1}{a} \cdot b = \frac{b}{a}
$$

$(E)$ admet une solution unique $x=\frac{b}{a}$ si et seulement si $a \ne 0$.

Ainsi la notion de matrice inverse $\mathbf{A^{-1}}$  dans l'ensemble des matrices carrées généralise la notion d'inverse  $\frac{1}{a}$ dans $\mathbb{R}$.
```

````{prf:example}
Soit le système:

$$
(S) :\left\{\begin{array}{ccccccc} x & + & 2y & -& 3z &=& 16  \\ 2x & - & 2y & - & z &=& 6  \\ 3x & + & 2y & + & z &=& 4  \\\end{array} \right. \Leftrightarrow \underbrace {\left(\begin{array}{ccc} 1& 2 & -3 \\ 2 & -2 & -1 \\ 3 & 2 & 1 \\\end{array} \right)}_{\mathbf{A}} \cdot \underbrace {\left(\begin{array}{c} x \\ y \\ z \\\end{array} \right)}_{\mathbf{X}}= \underbrace {\left(\begin{array}{c} 16 \\ 6 \\ 4 \\\end{array} \right)}_{\mathbf{B}}
$$

On inverse la matrice du système :

$$
\mathbf{A^{-1}} = \left(\begin{array}{ccc} 0 & 1/5 & 1/5 \\ 1/8 & -1/4 & 1/8 \\ -1/4 & -1/10 & 3/20 \\\end{array} \right)
$$

On calcule :

$$
\mathbf{X} = \mathbf{A^{-1}} \cdot \mathbf{B} = \left(\begin{array}{ccc} 0 & 1/5 & 1/5 \\ 1/8 & -1/4 & 1/8 \\ -1/4 & -1/10 & 3/20 \\\end{array} \right) \cdot \left(\begin{array}{c} 16 \\ 6 \\ 4 \\\end{array} \right) = \left(\begin{array}{c} 2 \\ 1 \\ -4 \\\end{array} \right)
$$

On en déduit : $x=2, \: y=1, \: z=-4$
````

```{danger}
L'utilisation de $\mathbf{A^{-1}}$  pour résoudre un système n'a qu'un intérêt théorique parce que les temps de calculs sont trop longs. 

Pratiquement, on préfère résoudre les systèmes linéaires par d'autres méthodes, la méthode de Gauss.

Cette méthode est très souvent plus rapide et permet en outre de résoudre aussi le système quand $A$ n'est pas inversible.
```

### Résolution de système linéaire par la méthode du pivot de Gauss

```{note}
Cette section est empruntée à [bibmath.net](https://www.bibmath.net/dico/index.php?action=affiche&quoi=./g/gausspivot.html)
```



```{important}
La méthode du pivot de Gauss est une méthode pour transformer un système en un autre système équivalent
(ayant les mêmes solutions) qui est **triangulaire** et est donc facile à résoudre. 
Les opérations autorisées pour transformer ce système sont:

* échange de deux lignes. 
* multiplication d'une ligne par un nombre non nul. 
* addition d'un multiple d'une ligne à une autre ligne. 
```

````{prf:example}
Soit le système:

$$
(S) :\left\{
    \begin{array}{cccccccc} 
    x & + & 2y & + & 2z &=& 2  &L1  \\ 
    x & + & 3y & - & 2z &=& -1  &L2\\
    3x & + & 5y & + & 8z &=& 8  &L3\\
    \end{array} \right. 
$$

On conserve la ligne $L1$, qui sert de **pivot** pour éliminer l'inconnue $x$ des autres lignes; 
pour cela, on retire $L1$ à $L2$, et $3$ fois $L1$ à $L3$. On obtient :

$$
(S) :\left\{
    \begin{array}{cccccccc} 
    x & + & 2y & + & 2z &=& 2  &L1  \\ 
     & + & y & - & 4z &=& -3  &L2 \leftarrow L2-L1 \\
     & - & y & + & 2z &=& 2  &L3 \leftarrow L3-3L1  \\
    \end{array} \right. 
$$

On conserve alors la ligne $L2$ qui sert de **pivot** pour éliminer $y$ de la troisième ligne; 
pour cela, on remplace la ligne $L3$ par $L3+L2$. On trouve :

$$
(S) :\left\{
    \begin{array}{cccccccc} 
    x & + & 2y & + & 2z &=& 2  &L1  \\ 
     & + & y & - & 4z &=& -3  &L2 \\
     &  &  & - & 2z &=& -1  &L3 \leftarrow L3+L2  \\
    \end{array} \right. 
$$

Ce dernier système, triangulaire, est facile à résoudre : la dernière ligne donne $z=\frac{1}{2}$ 

En reportant la valeur de $z$ dans $L2$, la deuxième ligne donne $y$:

$$
\begin{eqnarray}
y &=&-3+4z \\
&=& -3+2 \\
&=& -1
\end{eqnarray}
$$

Enfin, en reportant les valeurs de $y$ et $z$ dans la première ligne, on trouve:

$$
\begin{eqnarray}
x &=& -2y-2z+2 \\
&=& -2 (-1)-2(\frac{1}{2})+2
&=& 3
\end{eqnarray}
$$

D'où finalement la solution unique: $x=3, ~y=-1, ~z=\frac{1}{2}$

On peut la réinjecter dans le système initial et vérifier que les 3 équations sont bien vérifiées.
````

```{note}
Pour aller plus loin, vous pouvez consulter {cite}`warin_1999`, la partie sur les systèmes linéaires d'équations [www](https://uel.unisciel.fr/physique/outils_nancy/outils_nancy_ch11/co/apprendre_ch11_18.html)
```


Exemples d'utilisation en GEII
-----------

Dans cette partie, on montre comment utiliser le calcul matriciel pour résoudre des problèmes de GEII.

* Dans l'exemple {ref}`exemple_circuit_courant`, on utilise une résolution de système linéaire pour trouver les courants dans un circuit électrique simple.
* Dans l'exemple {ref}`exemple_SFD` Série de Fourier Discrète (SFD), on interprète les coefficients fréquentiels de la SFD $X_k$ associés à un signal $x_n$ comme un produit matriciel $\mathbf{A}\mathbf{x}$.
* Dans l'exemple {ref}`exemple_filtrage` on calcule la sortie d'un filtre dont l'entrée est connue, grâce à une multiplication matricielle.

(exemple_circuit_courant)=
### Calcul des courants dans un circuit

`````{admonition} Problème GEII
* **Le problème**: calculer des courants dans un circuit résistif où les potentiels sont connus.
* **L'interprétation matricielle**: résolution d'un système matriciel $\mathbf{A}\mathbf{x}=\mathbf{b}$.
`````

```{note}
Cet exemple est tiré du cours {cite}`warin_1999`, accessible sur [unisciel](https://uel.unisciel.fr/physique/outils_nancy/outils_nancy_ch11/co/sexercer_11_2.html).
```

Soit le circuit ci-dessous (pont de Wheatstone):

![pont_R](fig/pont_wheatstone_R.gif)

On décide de l'orientation des courants comme suit:

![pont_I](fig/pont_wheatstone_I.gif)

On écrit la loi des noeuds:

* noeud A: $I=I_1+I_2$
* noeud B: $I_1=I_3+I_4$ 
* noeud C: $I_5=I_2+I_3$
* noeud D: $I=I_4+I_5$

On écrit la loi des mailles:

* maille ABCA: $R_1 I_1 + R_3 I_3-R_2 I_2=0$
* maille BDCB: $R_4 I_4 -R_5 I_5 -R_3 I_3=0$
* maille ACDA: $R_2 I_2+R_5 I_5=E$ 

On substitue $I_1$ et $I_5$ dans les équations de maille:

$$
(S) :\left\{\begin{array}{ccc} 
R_1 (I_3+I_4) + R_3 I_3 -R_2 I_2 &=& 0 \\
 R_4 I_4 - R_5(I_2+I_3)  - R_3 I_3 &=& 0 \\
  R_2 I_2 + R_5 (I_2+I_3) &=& E  \\
\end{array} \right.
$$

```{note}

* Les **inconnues** sont $I_2, I_3, I_4$
* Les autres grandeurs ($R_1, \ldots, R_5$ et $E$) sont des **paramètres**, leur valeur numérique est supposée connue.
* On a un système de 3 équations à 3 inconnues.
```

On ordonne le système:

$$
(S) :\left\{\begin{array}{ccc} 
-R_2 I_2 + (R_1 +R_3) I_3 +R_1 I_4 +  &=& 0 \\
 - R_5 I_2  - (R_3+R_5) I_3 + R_4 I_4  &=& 0 \\
  (R_2 +R_5) I_2 + R_5 I_3 &=& E  \\
\end{array} \right.
$$

Finalement on obtient le système linéaire sous **forme matricielle**, de taille $3 \times 3$:

$$
\underbrace { 
    \left(\begin{array}{ccc}
    -R_2 & (R_1 +R_3)  & R_1 \\
     - R_5 & - (R_3+R_5) & R_4\\
     (R_2 +R_5) & R_5 & 0\\
    \end{array} \right)
}_{\mathbf{A}}
\cdot
\underbrace { 
    \left(
    \begin{array}{c} 
    I_2 \\
    I_3\\
    I_4\\
    \end{array} \right) 
}_{\mathbf{x}}
= 
\underbrace { 
    \left(
    \begin{array}{c} 
    0 \\
    0\\
    E\\
    \end{array} \right) 
}_{\mathbf{b}}
$$

```{note}
* Les **inconnues** ont été regroupées dans le vecteur $\mathbf{x}$.
* La matrice $\mathbf{A}$ est constituée de **paramètres** dont les valeurs sont **connues**.
* Le vecteur $\mathbf{b}$ est aussi connu.
```

`````{admonition} Méthode
:class: tip

Pour un choix particulier des valeurs des paramètres, il ne reste plus qu'à:

* vérifier que le déterminant n'est pas nul.
* résoudre avec une méthode au choix (matrice inverse ou pivot de Gauss).
`````

````{prf:example}
**Avec SAGE**

Supposons:

$$
\left\{\begin{array}{c} 
R_1  &=& 1 ~k\Omega \\
R_2  &=& 1 ~k\Omega  \\
R_3  &=& 1 ~k\Omega  \\
R_4  &=& 1 ~k\Omega  \\
R_5  &=& 1 ~k\Omega  \\
E  &=& 2 ~V \\
\end{array} \right.
$$

On rentre le système $\mathbf{A}\cdot \mathbf{x}=\mathbf{b}$  dans un logiciel de calcul scientifique:

```
R1=R2=R3=R4=R5 = 10^3
E=2
A = matrix(Z,3,3,[-R2, (R1 +R3), R1, -R5, -(R3+R5), R4, (R2 +R5), R5, 0]);
b = matrix(Z,3,1, [0,0,E]) 
```

La notation `matrix(Z,3,3,[-R2, ..., 0])` signifie qu'on crée une matrice à valeur dans $\mathbb{Z}$, de taille $3 \times 3$, 
dont les coefficients sont rentrés ligne après ligne dans le vecteur `[-R2, ..., 0]`. 


Pour obtenir le déterminant on tapera:

```
A.det()
```
On obtient:

```
8000000000
```

Le déterminant est non nul donc le système admet une unique solution.
La solution est obtenue avec la commande:

```
A.solve_right(b)
```

Soit: 

$I_2=1 ~mA, ~I_3=0 ~mA, ~I_4=1 ~mA$

Cette valeur est cohérente: quand le pont est équilibré (mêmes résistances dans les 2 bras du pont), il n'y a pas de courant
dans la branche du milieu.
````
```{note}
En réalité, dans les logiciels tels que Proteus ou Spice, c'est bien une résolution de système matriciel
qui est effectuée, mais à partir d'une autre mise en équation, qui s'appelle **analyse nodale**,
voir [wikipedia](https://en.wikipedia.org/wiki/Nodal_analysis)
```


`````{admonition} Résumé

* **Le problème GEII**: calculer les courants dans un circuit.
* **L'interprétation matricielle**: résoudre le système linéaire $\mathbf{A}\mathbf{x}=\mathbf{b}$.
`````


(exemple_SFD)=
### Série de Fourier Discrète (SFD)


`````{admonition} Problème GEII

* **Le problème**:  calculer les $X_k$. (trouver une composante périodique dans un signal temporel inconnu).
* **L'interprétation matricielle**: calcul d'un produit matriciel $\mathbf{A}\mathbf{x}$.
`````

```{note}
Cet exemple s'inspire du livre {cite}`unpingco_python_2014`.
```


`````{admonition} Rappel
:class: tip

En OML3 on a vu la notion de Série de Fourier Discrète (SFD).

Le **signal** temporel qu'on mesure, noté $x[n]$, est un **tableau** de réels de longueur $N$, avec $0\leq n < N$.
$n$ est l'indice de l'**instant de temps**.

$\frac{k}{N}$ est la **fréquence**. $k$ est le multiplicateur de la fréquence de base $\frac{1}{N}$.

Les **coefficients fréquentiels de la SFD** sont notés $X_k$, avec $0\leq k < N$.



$$
\begin{align}
    X_k &= \frac{1}{N} \sum_{n=0}^{N-1} x[n] e^{-jk\frac{2\pi}{N}  n  }\\
\end{align}
$$ 

```{important}
**Interprétation de $X_k$**

Les coefficients $X_k$ sont importants en GEII et dans tous les domaines liés au signal et à l'image.

Si le scalaire $X_k$ a un module important, cela signifie que dans le signal temporel $\{x[n], ~0\leq n < N\}$ il y a une forte composante périodique sinusoïdale de fréquence $\frac{k}{N}$.
```

On peut **comparer** ceci avec l'écriture complexe des coefficients de la DSF (Décomposition en Série de Fourier) vue au cours du premier amphi (révisions):

$$
\begin{eqnarray}
c_k &=& \frac{1}{T} \int_{[T]} x(t) e^{-jk \frac{2\pi}{T} t} dt 
\end{eqnarray}
$$

Le terme $ \sum_{n=0}^{N-1} x[n] e^{-jk\frac{2\pi}{N}  n  }$ dans $X_k$ s'interprète comme le **produit scalaire** de deux vecteurs:
 
 * le vecteur $[x[0], x[1],\ldots, x[N-1]]$ 
 * le vecteur $u_k=[e^{-jk\frac{2\pi}{N} \times 0  }, e^{-jk\frac{2\pi}{N} \times 1  }, \ldots, e^{-jk\frac{2\pi}{N} \times (N-1)} ]$ vu ci-dessus.

`````

```{important}
**Interprétation matricielle du calcul de la SFD**

En GEII on a besoin de calculer les $X_k$ pour $0\leq k < N$. 

Ceci revient au **produit matriciel** suivant: 

$$
\left[\begin{array}{c}
X_0\\ 
X_{1}\\
\vdots\\
X_{N-1}\\
\end{array}\right]
=
\left[\begin{array}{ccccc} 
e^{-j \color{blue}0 \color{black} \frac{2\pi}{N} \times \color{red}0  } & e^{-j 0 \frac{2\pi}{N} \times \color{red}1  } &  \ldots &  e^{-j 0 \frac{2\pi}{N} \times \color{red}{(N-2)}} & e^{-j 0 \frac{2\pi}{N} \times \color{red}{(N-1)}} \\
\vdots &&&& \vdots\\
e^{-j \color{blue}k \color{black} \frac{2\pi}{N} \times 0  } & e^{-jk\frac{2\pi}{N} \times 1  } &  \ldots &  e^{-jk\frac{2\pi}{N} \times (N-2)} & e^{-jk\frac{2\pi}{N} \times (N-1)} \\
\vdots &&&& \vdots\\
e^{-j \color{blue}{(N-1)} \color{black} \frac{2\pi}{N} \times 0  } & e^{-j (N-1) \frac{2\pi}{N} \times 1  } &  \ldots &  e^{-j (N-1) \frac{2\pi}{N} \times (N-2)} & e^{-j (N-1) \frac{2\pi}{N} \times (N-1)} 
\end{array} \right]
\cdot
\left[\begin{array}{c}
x[0]\\ 
x[1]\\
\vdots\\
x[N-1]\\
\end{array}\right]
$$

* Le long d'une ligne, le temps ($n$) augmente, en $\color{red}{\textrm{rouge}}$.
* Le long d'une colonne, le multiplicateur de fréquence ($k$) augmente, en $\color{blue}{\textrm{bleu}}$.
* Une ligne de la matrice correspond à une **fréquence** donnée.
* Une colonne correspond à un **instant de temps** donné.

On notera $\mathbf{U}$ la **matrice** qui contient tous les vecteurs $u_k$ empilés ligne par ligne.

Et on représente schématiquement ce produit matriciel comme suit:

![TFD](fig/TFD_matricielle.svg)


Lorsqu'on calcule cette multiplication d'une matrice par un vecteur, la **dimension temporelle disparaît**.
Il ne reste plus que les fréquences. C'est cohérent avec le fait que la SFD est une transformation du domaine **temporel** vers
le domaine **fréquentiel**.
```

````{prf:example}
**Python**

On pose $N=16$, pour $k \in [0,8[$ on va calculer les vecteurs  $u_k=[e^{-jk\frac{2\pi}{N} \times 0  }, e^{-jk\frac{2\pi}{N} \times 1  }, \ldots, e^{-jk\frac{2\pi}{N} \times (N-1)} ]$ vus ci-dessus et les empiler dans une matrice dont chaque ligne est un vecteur $u_k$.

**1) Calcul sous Python de la matrice $\mathbf{U}$ contenant les vecteurs $u_k$**

```python
N = 16
k_vec= np.arange(int(N/2))                              # [0,1...,7]
n = np.arange(N)                                             # [0,...,15]
U = np.zeros((int(N/2), N),dtype='complex')    # matrice 8*16 pleine de zeros
for k in k_vec:
    uk = np.exp(-1j* 2*np.pi/N *k *n)
    U[k,:] = uk
    ax[k].plot(np.real(uk))    
```


%```{glue:figure} fig_matrix_example_DFT
%:figwidth: 700px
%:name: "fig_matrix_example_DFT"
%```
```{glue} fig_matrix_example_DFT
:doc: notebooks.ipynb
```
![fig_matrix_example_DFT](fig/temp/fig_matrix_example_DFT.png)


Observations:

* les abscisses représentent le temps $n$. Les ordonnées représentent la partie réelle des éléments de $u_k$.
* la première ligne représente le vecteur $u_k$ pour $k=0$, il s'agit bien d'un vecteur dont tous les termes sont identiques.
* plus on descend, plus $k$ augmente, et plus la fréquence $f_k=\frac{k}{N}$ augmente.



**2) Multiplication de $\mathbf{U}$ par un signal inconnu**

Puis on va multiplier cette matrice $\mathbf{U}$ par un vecteur contenant un signal inconnu, pour voir quelle est la valeur de $k$ qui obtient la plus forte réponse.

Pour le signal inconnu on va prendre le **conjugué** de l'un des vecteurs $u_k$ ci-dessus, pour $k=3$:

```python
N=16
k=3
n = np.arange(N)                        # [0,...,15]
x = np.exp(1j* 2*np.pi/N *k *n)  # x est le conjugué de u_3 
```

On multiplie la matrice $\mathbf{U}$ par $x$, puis on calcule le module:

```python
y=U.dot(x)
y_module = np.abs(y)    # np.abs calcule le module si y complexe
```

On trouve:

```python
y_module=
[1.1e-15, 1.3e-15, 2.2e-15, 1.6e+01, 1.2e-15, 7.5e-15, 1.2e-15, 5.4e-15])
```

On affiche ce vecteur:

```python
fig,ax = plt.subplots(1,1,figsize=(10,5))
ax.plot(y_module)
ax.set_xlabel("n")
ax.set_ylabel("|y|")
```

%```{glue:figure} fig_matrix_example_DFT_2
%:figwidth: 700px
%:name: "fig_matrix_example_DFT_2"
%```
```{glue} fig_matrix_example_DFT_2
:doc: notebooks.ipynb
```
![fig_matrix_example_DFT_2](fig/temp/fig_matrix_example_DFT_2.png)


Conclusions:

* C'est bien la composante $k=3$ la plus forte.
* On a donc retrouvé la composante périodique dans le signal temporel inconnu.

````


`````{admonition} Résumé

* **Le problème GEII**: calculer la SFD d'un signal, trouver une composante périodique dans un signal temporel inconnu.
* **L'interprétation matricielle**: calcul d'un produit matriciel $\mathbf{A}\mathbf{x}$.
`````



+++ some text

<h3> Filtrage </h3>

(exemple_filtrage)=
### Filtrage

`````{admonition} Problème GEII
* **Le problème**: nettoyer un signal bruité.
* **L'interprétation matricielle**: calcul d'un produit matriciel $\mathbf{A}\mathbf{x}$.
`````

```{note}
Cet exemple est tiré du cours {cite}`smith_2007`, et en particulier de la page [www](https://ccrma.stanford.edu/~jos/fp/Introduction_I.html) et les suivantes.
```

`````{admonition} Rappel
:class: tip

En OML3, on a traité le **filtrage de signaux** discrets à l'aide de la transformée en Z (passe-haut, passe-bas).

On avait noté $\{x_k, ~k \geq 0\}$ et $\{y_k, ~k \geq 0\}$ deux suites de nombres. 

* la première est l'entrée du filtre, par exemple une mesure bruitée.
* la seconde est la sortie du filtre, par exemple la mesure débruitée.

Pour un filtre **filtre non récursif**, aussi appelé RIF (réponse impulsionnelle finie), ou FIR en anglais, on avait la définition:

$$
\begin{eqnarray}
y_k &=& \sum_{i=0}^M b_i x_{k-i} \\
&=& b_0 x_k + b_1 x_{k-1}+b_2 x_{k-2}+\ldots
\end{eqnarray}
$$

**NB**  les coefficients $b_0, b_1, \ldots$ sont **constants**, ce sont les **paramètres** du filtre.
`````

```{important}

Dans cette section, on montre que les filtres non-récursifs peuvent se mettre facilement sous forme d'un **produit matriciel** entre une matrice et un vecteur
$\mathbf{A} \cdot \mathbf{x} = \mathbf{y}$ comme ci-dessus.

Il y a donc une différence avec l'exemple précédent ({ref}`exemple_circuit_courant`):

* $\mathbf{x}$ est **connu**: c'est le signal d'entrée.
* $\mathbf{y}$ est **inconnu**: c'est le signal de sortie.
* on ne résout pas un système de $n$ équations à $n$ inconnues, mais on fait un simple **produit matriciel** pour trouver $\mathbf{y}$.

```


````{prf:example}
**Filtre non-récursif sous forme matricielle**

Soit le filtre non-récursif défini pour $k\geq 0$ par l'équation de récurrence: 

$$
\begin{eqnarray}
y_k &=& \sum_{i=0}^2 b_i x_{k-i} \\
&=& b_0 x_k + b_1 x_{k-1}+b_2 x_{k-2}
\end{eqnarray}
$$

On a donc:

$$
\begin{eqnarray}
y_0 &=&  b_0 x_{0} \\
y_1 &=&  b_0 x_{1} + b_1 x_{0} \\
y_2 &=&  b_0 x_{2} + b_1 x_{1} + b_2 x_{0} \\
\end{eqnarray}
$$

Ecrivons la suite en entrée $\{x_k, ~k \geq 0\}$ comme un vecteur, en se limitant aux 3 premiers termes:

$$
\mathbf{x}= \left[\begin{array}{c}
x_{0}\\ 
x_{1}\\
x_{2}\\
\end{array}\right].
$$

Pour modéliser le filtre, on écrit:

$$
\mathbf{H}= \left[\begin{array}{ccc}
b_{0} & 0 & 0\\
b_{1} & b_{0} & 0\\
b_{2} & b_{1} & b_{0}
\end{array}\right]
$$

Pour écrire sous forme matricielle l'application à notre filtre de l'entrée $\mathbf{x}$, on pose : 

$$
\mathbf{H}\mathbf{x} = \left[\begin{array}{ccc}
b_{0} & 0 & 0\\
b_{1} & b_{0} & 0\\
b_{2} & b_{1} & b_{0}
\end{array}\right]
\cdot
\left[\begin{array}{c}
x_{0}\\ 
x_{1}\\
x_{2}\\
\end{array}\right]
=
\left[\begin{array}{c}
b_0 x_{0}\\ 
b_0 x_{1} + b_1 x_{0} \\
b_0 x_{2} + b_1 x_{1} + b_2 x_{0} \\
\end{array}\right]
=
\left[\begin{array}{c}
y_{0}\\ 
y_{1}\\
y_{2}\\
\end{array}\right]
$$

**Conclusion:** on retombe bien sur le résultat au début de l'exemple, à partir de l'équation de récurrence.
````

```{note}
* On voit donc que le **filtrage** peut parfois s'écrire sous forme d'un **produit matriciel**.
* La représentation des filtres RIF/FIR sous forme matricielle est intéressante pour la pédagogie,
mais **en pratique**, les logiciels scientifiques utilisent plutôt l'équation de récurrence, sous une forme modifiée.
Vous pouvez regarder la page Scipy pour avoir un example [www](https://docs.scipy.org/doc/scipy/tutorial/signal.html#difference-equation-filtering).
```

````{prf:example}
**Python**

Soit le filtre à moyenne glissante (non récursif) suivant $y_k = \frac{1}{M} \sum_{i=0}^{M-1}  x_{k-i}$ pour $M=2$. 
On a donc posé $\forall i \geq 0, ~b_i = \textrm{cst} = \frac{1}{M}$. 

Pour $k \geq 0$, on a:

$$
\begin{eqnarray}
y_k & &=& \frac{1}{2} (  x_k +  x_{k-1})
\end{eqnarray}
$$

Sous forme matricielle, l'opération de filtrage $\mathbf{y} = \mathbf{H}\mathbf{x}$ s'écrit:

$$
\mathbf{H}\mathbf{x} = \frac{1}{2} 
\left(\begin{array}{ccccccc} 
1 & 0 & & \cdots &  0  & 0&0\\
1 & 1 & & \cdots &   0 &0 &0\\
0 & 1 &1 & \cdots & 0 &0 &0\\
\vdots & \vdots  & \vdots  & \ddots & & \vdots & \vdots  \\
0 & 0 & 0 & \cdots & 1 & 0 & 0 \\
0 & 0 & 0 & \cdots & 1 & 1 & 0 \\
0 & 0 & 0 & \cdots & 0& 1 & 1 
\end{array} \right)
\cdot
\left[\begin{array}{c}
x_{0}\\ 
x_{1}\\
\vdots\\
x_{p}\\
\end{array}\right]
=
\left[\begin{array}{c}
y_{0}\\ 
y_{1}\\
\vdots\\
y_{p}\\
\end{array}\right]
$$

Soit, avec Python:

```python
# longueur du vecteur en entrée 
p = 100                 
# bruit
x = np.random.randn(p)   
# on soustrait deux matrices triangulaires pour obtenir une matrice "bande"
H = np.tri(p,p, dtype=int)-np.tri(p,p,-2,dtype=int) 
# on n'oublie pas le facteur
H = 0.5 * H
# produit matriciel:
y = H.dot(x)
```

Et le résultat du filtrage:

%```{glue:figure} fig_matrix_example_filter
%:figwidth: 700px
%:name: "fig_matrix_example_filter"
%```
```{glue} fig_matrix_example_filter
:doc: notebooks.ipynb
```
![fig_matrix_example_filter](fig/temp/fig_matrix_example_filter.png)

La même chose en augmentant $M$, on s'attend à un effet de lissage:

%```{glue:figure} fig_matrix_example_filter2
%:figwidth: 700px
%:name: "fig_matrix_example_filter2"
%```
```{glue} fig_matrix_example_filter2
:doc: notebooks.ipynb
```
![fig_matrix_example_filter2](fig/temp/fig_matrix_example_filter2.png)

Cette méthode matricielle est-elle bien équivalente à l'utilisation de l'équation de récurrence ?
Pour le savoir on utilise la fonction Python/Scipy `lfilter` (voir OML3 cours magistral ainsi que TP associé).

%```{glue:figure} fig_matrix_example_filter3
%:figwidth: 700px
%:name: "fig_matrix_example_filter3"
%```
```{glue} fig_matrix_example_filter3
:doc: notebooks.ipynb
```
![fig_matrix_example_filter3](fig/temp/fig_matrix_example_filter3.png)


On retrouve bien **le même résultat** en sortie du filtre, avec la fonction `lfilter` qui utilise l'équation de récurrence,
ou par la méthode matricielle.

````

`````{admonition} Résumé

* **Le problème GEII**: filtrer un signal.
* **L'interprétation matricielle**: calcul d'un produit matriciel $\mathbf{H}\mathbf{x}$.
`````

